/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/astroaccelerate/Sps.h"
#include "cheetah/ddtr/astroaccelerate/Ddtr.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/SpCcl.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/cuda_utils/nvtx.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include "panda/Error.h"
#include <memory>
#include <map>
#include <algorithm>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

namespace detail {

template<class SpsTraits, typename Enable>
template<typename SpHandler, typename OtherBufferType>
std::shared_ptr<typename SpsTraits::DmTrialsType> Sps<SpsTraits, Enable>::operator()(panda::PoolResource<cheetah::Cpu>&
                    , OtherBufferType&
                    , SpHandler&
                    )
{
    throw panda::Error("astroaccelerate::Sps can only handle uint8_t - please deactivate this algorithm in your config");
}

 // --------------- 8bit implemantation ----------------------
template<class SpsTraits>
using EnableIfIsUint8T = typename std::enable_if<std::is_same<uint8_t, typename SpsTraits::value_type>::value
                                                >::type;

template<class SpsTraits>
class Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>> : private utils::AlgorithmBase<Config, sps::Config>
{
    private:
        typedef utils::AlgorithmBase<Config, sps::Config> BaseT;
        typedef sps::CommonTypes<sps::Config, uint8_t> Common;

    public:
        // mark the architecture this algo is designed for
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<3, 5, panda::nvidia::giga> ArchitectureCapability;
        typedef ddtr::astroaccelerate::DedispersionPlan<SpsTraits> DedispersionPlan;

    public:
        typedef data::TimeFrequency<Cpu, uint8_t> TimeFrequencyType;

    private:
        typedef typename Common::BufferType BufferType;

    public:
        Sps(sps::Config const& config);
        Sps(Sps&&) = default;
        Sps(Sps const&) = delete;

        /**
         * @brief call the dedispersion/sps algorithm using the provided device
         */
        template<typename SpHandler>
        std::shared_ptr<typename SpsTraits::DmTrialsType> operator()(panda::PoolResource<cheetah::Cuda>&, BufferType&, SpHandler&);

        /*
         * catch non uint8_t data
         */
        template<typename SpHandler, typename OtherBufferType>
        std::shared_ptr<typename SpsTraits::DmTrialsType> operator()(panda::PoolResource<cheetah::Cuda>&, OtherBufferType&, SpHandler&);

        /**
         * @brief set the dedispersion plan
         */
        void plan(DedispersionPlan const& plan);

    private:
        ddtr::astroaccelerate::Ddtr<SpsTraits> _ddtr;
        std::map<unsigned, SpsCuda<SpsTraits>> _cuda_runner;
};

template<class SpsTraits>
template<typename SpHandler>
std::shared_ptr<typename SpsTraits::DmTrialsType> Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::operator()(panda::PoolResource<panda::nvidia::Cuda>& gpu
                    , BufferType& agg_buf
                    , SpHandler& sp_handler
                    )
{
    auto it = this->_cuda_runner.find(gpu.device_id());
    if(it==this->_cuda_runner.end()) {
        it = (_cuda_runner.insert(std::make_pair(gpu.device_id(), SpsCuda<SpsTraits>(_algo_config)))).first;
    }
    return (*it).second(gpu, agg_buf, sp_handler, _ddtr);
}

template<class SpsTraits>
template<typename SpHandler, typename OtherBufferType>
std::shared_ptr<typename SpsTraits::DmTrialsType> Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::operator()(panda::PoolResource<cheetah::Cuda>&
                    , OtherBufferType&
                    , SpHandler&
                    )
{
    throw panda::Error("astroaccelerate::Sps can only handle uint8_t - please deactivate this algorithm in your config");
}

template<class SpsTraits>
Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::Sps(sps::Config const& config)
    : BaseT(config.astroaccelerate_config(), config)
    , _ddtr(config.ddtr_config())
{
}

template<class SpsTraits>
void Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::plan(DedispersionPlan const& plan)
{
    _ddtr.plan(plan);
}

} // namespace detail

template<class SpsTraits>
Sps<SpsTraits>::Sps(sps::Config const& config)
    : BaseT(config)
{

}

template<class SpsTraits>
template<typename SpHandler, typename BufferType>
std::shared_ptr<typename SpsTraits::DmTrialsType> Sps<SpsTraits>::operator()(panda::PoolResource<Architecture>& dev, BufferType& buf, SpHandler& sp_h)
{
    return static_cast<BaseT&>(*this)(dev, buf, sp_h);
}


} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska
