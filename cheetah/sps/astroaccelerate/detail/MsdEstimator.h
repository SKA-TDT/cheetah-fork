#ifndef SKA_CHEETAH_SPS_ASTROACCELERATE_MSDESTIMATOR_H
#define SKA_CHEETAH_SPS_ASTROACCELERATE_MSDESTIMATOR_H

#include "cheetah/sps/astroaccelerate/detail/SpsCuda.cuh"
#include "cheetah/sps/astroaccelerate/Config.h"
#include "cheetah/sps/astroaccelerate/SpsStrategy.h"
#include "cheetah/ddtr/astroaccelerate/Ddtr.h"
#include "cheetah/ddtr/astroaccelerate/DedispersionStrategy.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

/**
 * @brief MSD (mean, standard deviation, nelements(total number of elements used in mean std estimation after outlier rejection))
 * estimation for the astroaccelerate
 */
template <typename SpsTraits>
class MsdEstimator
{
    private:
        typedef typename SpsTraits::value_type DataType;

    public:
        typedef data::TimeFrequency<Cpu, DataType> TimeFrequencyType;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;

    public:
        MsdEstimator(ddtr::astroaccelerate::DdtrProcessor<SpsTraits>& ddtr_processor
                   , sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& sps_strategy
                   , int current_range);
        ~MsdEstimator();

    private:
        class MemoryEstimator
        {
            private:
                typedef typename SpsTraits::value_type DataType;
            public:
                MemoryEstimator(sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& sps_strategy, int current_range);
                ~MemoryEstimator();

                /**
                * @brief resize of the boxcar MSD values
                */
                size_t boxcar_msd_values_size() const;

                /**
                * @brief Size of the work area (temporary area) required for estimating MSD
                */
                size_t msd_workarea_size() const;

            private:
                sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& _sps_strategy;
                size_t _boxcar_msd_values_size;
                size_t _msd_workarea_size;
        };

    public:

        /**
        * @brief return the device error storing the MSD values
        */
        panda::nvidia::CudaDevicePointer<float>& msd_interpolated();

        /**
        * @brief holder for the decimated time samples
        * @details this needs to be passed to astroaccelerate,
        *          although it is not used for sps (the size is set to zero)
        */
        panda::nvidia::CudaDevicePointer<float>& msd_dit();

    private:
        sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& _sps_strategy;
        int _current_range;
        MemoryEstimator _memory_size;
        panda::nvidia::CudaDevicePointer<float> _msd_interpolated;
};

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

#include "cheetah/sps/astroaccelerate/detail/MsdEstimator.cpp"
#endif // SKA_CHEETAH_SPS_ASTROACCELERATE_MSDESTIMATOR_H
