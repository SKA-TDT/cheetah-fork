/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

template<typename SpsTraits>
MsdEstimator<SpsTraits>::MsdEstimator(ddtr::astroaccelerate::DdtrProcessor<SpsTraits>& ddtr_processor, sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& sps_strategy, int current_range)
    : _sps_strategy(sps_strategy)
    , _current_range(current_range)
    , _memory_size(_sps_strategy, _current_range)
    , _msd_interpolated(_memory_size.boxcar_msd_values_size()/sizeof(float))
{
    // Definition of some local variables
	float current_start_time = 0.0;
	size_t number_of_samples = sps_strategy.current_time_samples();
	size_t ndms = sps_strategy.strategy().ndms()[current_range];
	std::vector<int> h_boxcar_widths = sps_strategy.boxcar_widths();

	double msd_time=0, dit_time=0, msd_only_time=0;
    cudaDeviceSynchronize();

	{
		panda::nvidia::CudaDevicePointer<float> temporary_workarea(_memory_size.msd_workarea_size()/sizeof(float));
		::astroaccelerate::MSD_plane_profile(_msd_interpolated()
											, thrust::raw_pointer_cast(&*(ddtr_processor.gpu_strategy().ddtr_output().begin()))
											, nullptr
											, temporary_workarea()
											, false
											, number_of_samples
											, ndms
											, &h_boxcar_widths
											, current_start_time
											, _current_range
											, sps_strategy.or_sigma_multiplier()
											, sps_strategy.enable_outlier_rejection()
											, false
											, &msd_time
											, &dit_time
											, &msd_only_time);
	} // clear temporary_workarea

}

template<typename SpsTraits>
MsdEstimator<SpsTraits>::~MsdEstimator()
{

}

template<typename SpsTraits>
panda::nvidia::CudaDevicePointer<float>& MsdEstimator<SpsTraits>::msd_interpolated()
{
    return _msd_interpolated;
}

template<typename SpsTraits>
MsdEstimator<SpsTraits>::MemoryEstimator::~MemoryEstimator()
{
}

template<typename SpsTraits>
MsdEstimator<SpsTraits>::MemoryEstimator::MemoryEstimator(sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& sps_strategy, int current_range)
    : _sps_strategy(sps_strategy)
{
    // Definition of some local variables
	size_t number_of_samples = sps_strategy.current_time_samples();
	size_t ndms = sps_strategy.strategy().ndms()[current_range];
	std::vector<int> boxcar_widths = sps_strategy.boxcar_widths();
    size_t temp;
	//-------------------------------------------------------------------------
	//------------ Using msd_plane_profile
	::astroaccelerate::Get_MSD_plane_profile_memory_requirements(&_boxcar_msd_values_size
																, &temp
																, &_msd_workarea_size
																, number_of_samples
																, ndms
																, &boxcar_widths);
}

template<typename SpsTraits>
size_t MsdEstimator<SpsTraits>::MemoryEstimator::boxcar_msd_values_size() const
{
    return _boxcar_msd_values_size;
}

template<typename SpsTraits>
size_t MsdEstimator<SpsTraits>::MemoryEstimator::msd_workarea_size() const
{
    return _msd_workarea_size;
}

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska
