#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include <vector>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

void call_kernel_THR_GPU_WARP(const dim3 &grid_size, const dim3 &block_size,
					float const* __restrict__ d_input, ushort *d_input_taps, float *d_output_list,
					int *gmem_pos, float threshold, int nTimesamples, int offset, int shift,
					int max_list_size, int DIT_value, float sampling_time,
					float inBin, float start_time);

void call_kernel_dilate_peak_find(const dim3 &grid_size, const dim3 &block_size
                                , const float *d_input, ushort* d_input_taps
                                , float *d_peak_list, const int width
                                , const int height, const int offset
                                , const float threshold, int max_peak_size
                                , int *gmem_pos, int shift, int DIT_value
                                , float sampling_time, float inBin, float start_time);



} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska


namespace astroaccelerate {

// extern declarations for astroaccelerate library kernels

extern void Get_MSD_plane_profile_memory_requirements(size_t *MSD_profile_size_in_bytes, size_t *MSD_DIT_profile_size_in_bytes, size_t *workarea_size_in_bytes, size_t primary_dimension, size_t secondary_dimension, std::vector<int> *boxcar_widths);

extern void MSD_plane_profile(float *d_MSD_interpolated, float *d_input_data, float *d_MSD_DIT_previous
                            , float *workarea, bool high_memory, size_t primary_dimension
                            , size_t secondary_dimension, std::vector<int> *boxcar_widths
                            , float tstart, int range, float or_sigma_multiplier
                            , int enable_outlier_rejection, bool perform_continuous
                            , double *total_time, double *dit_time, double *MSD_time);

extern void call_kernel_SPDT_GPU_1st_plane(const dim3 &grid_size, const dim3 &block_size, float const *const d_input,
				    float *const d_bv_out, float *const d_decimated,
				    float *const d_output_SNR, ushort *const d_output_taps, float2 const *const d_MSD,
				    const int &nTimesamples, const int &nBoxcars, const int &dtm);

extern void call_kernel_SPDT_GPU_Nth_plane(const dim3 &grid_size, const dim3 &block_size, float const *const d_input,
				    float *const d_bv_in, float *const d_bv_out, float *const d_decimated,
				    float *const d_output_SNR, ushort *const d_output_taps, float2 const *const d_MSD,
				    const int &nTimesamples, const int &nBoxcars, const int &startTaps,
				    const int &DIT_value, const int &dtm);

extern __global__ void dilate_peak_find(const float *d_input, ushort* d_input_taps, float *d_peak_list, const int width, const int height, const int offset, const float threshold, int max_peak_size, int *gmem_pos, int shift, int DIT_value, float sampling_time, float inBin, float start_time);

extern __global__ void THR_GPU_WARP(float const* __restrict__ d_input, ushort *d_input_taps, float *d_output_list,
								int *gmem_pos, float threshold, int nTimesamples, int offset, int shift,
								int max_list_size, int DIT_value, float sampling_time,
								float inBin, float start_time);

} // namespace astroaccelerate
