#include "cheetah/sps/astroaccelerate/SpsStrategy.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/TimeFrequency.h"
#include <memory>
#include <algorithm>
#include <limits>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

IterationDetails::IterationDetails()
    : _decimated_timesamples(0)
    , _dtm(0)
    , _iteration(0)
    , _number_blocks(0)
    , _number_boxcars(0)
    , _output_shift(0)
    , _shift(0)
    , _start_taps(0)
    , _total_unprocessed(0)
    , _unprocessed_samples(0)
{

}

int IterationDetails::decimated_timesamples() const
{
    return _decimated_timesamples;
}

void IterationDetails::decimated_timesamples(int value)
{
    _decimated_timesamples = value;
}

int IterationDetails::dtm() const
{
    return _dtm;
}

void IterationDetails::dtm(int value)
{
    _dtm = value;
}

int IterationDetails::iteration() const
{
    return _iteration;
}

void IterationDetails::iteration(int value)
{
    _iteration = value;
}

int IterationDetails::number_blocks() const
{
    return _number_blocks;
}

void IterationDetails::number_blocks(int value)
{
    _number_blocks = value;
}

int IterationDetails::number_boxcars() const
{
    return _number_boxcars;
}

void IterationDetails::number_boxcars(int value)
{
    _number_boxcars = value;
}

int IterationDetails::output_shift() const
{
    return _output_shift;
}

void IterationDetails::output_shift(int value)
{
    _output_shift = value;
}

int IterationDetails::shift() const
{
    return _shift;
}

void IterationDetails::shift(int value)
{
    _shift = value;
}

int IterationDetails::start_taps() const
{
    return _start_taps;
}

void IterationDetails::start_taps(int value)
{
    _start_taps = value;
}

int IterationDetails::total_unprocessed() const
{
    return _total_unprocessed;
}

void IterationDetails::total_unprocessed(int value)
{
    _total_unprocessed = value;
}

int IterationDetails::unprocessed_samples() const
{
    return _unprocessed_samples;
}

void IterationDetails::unprocessed_samples(int value)
{
    _unprocessed_samples = value;
}

} //namespace ska
} //namespace cheetah
} //namespace sps
} //namespace astroaccelerate