/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/Fldo.h"
#include "cheetah/fldo/cuda/Fldo.h"
#include "cheetah/fldo/cpu/Fldo.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace fldo {

template<typename Handler, typename NumericalT, class ConfigType>
Fldo<Handler, NumericalT, ConfigType>::Fldo(ConfigType const& config, Handler& handler)
    : BaseT(config, handler)
{
}

template<typename Handler, typename NumericalT, class ConfigType>
inline
std::shared_ptr<panda::ResourceJob> Fldo<Handler, NumericalT, ConfigType>::operator()(std::vector<std::shared_ptr<TimeFrequencyType>>& tf_data, data::Scl const& scl_data)
{
    return static_cast<BaseT&>(*this)(tf_data, scl_data);
}

} // namespace fldo
} // namespace cheetah
} // namespace ska
