/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_CUDA_CONFIG_H
#define SKA_CHEETAH_FLDO_CUDA_CONFIG_H

#include "panda/ConfigActive.h"
#include "panda/Error.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {

/**
 * @brief
 * @details
 */

class Config : public panda::ConfigActive
{
        typedef panda::ConfigActive BaseT;

    public:
        Config();

        bool const& enable_split() const;

        /**
         * @brief return the number of phase bins
         */
        size_t const& phases() const;

        /**
         * @brief Configure the maximum number of phases used in folding
         */
        /**
         * void phases(size_t n)
         *
         * @param[in]  n     maximum number of phases used in folding
         *
         */
        void phases(size_t n);

        /**
         * @brief return the number of sub-integrations used in
         * the sum-up data
         */
        size_t const& nsubints() const;

        /**
         * @brief Configure the number of subints
         */
        /**
         * void nsubints(size_t n)
         *
         * @param[in]  n     the number of subints
         */
        void nsubints(size_t n);

        /**
         * @brief return the number of frequency sub-bands
         * summed up
         */
        size_t const& nsubbands() const;

        /**
         * @brief Configure the number of subbands
         */
        /**
         * void nsubbands(size_t n)
         *
         * @param[in]  n     the number of subbands
         */
        void nsubbands(size_t n);

        /**
         * fldo_input_check(fldo::cuda::Config const& config)
         * @brief
         * verify input config parameter are inside the bounds defined in CommonDefs.h.
         *
         * \return < 0 if error
         */
        int fldo_input_check(const fldo::cuda::Config&);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        bool _enable_split;     // to enable/disable the folding phase shift
        size_t _nsubints;       // the number of sub-integrations (default 64)
        size_t _nsubbands;      // the number of frequency sub-bands (default 64)
        size_t _phases;         // the number of phase bins (default 128)

};


} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_FLDO_CUDA_CONFIG_H
