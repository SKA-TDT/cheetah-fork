#!/bin/bash

# C.Baffa 20170623
# Procedure to get remotely testvectors from Arcetri repo
#

# Linux
wget=/usr/bin/wget
tar=/bin/tar
md5sum=/usr/bin/md5sum

# FreeBSD
#wget=/usr/local/bin/wget
#tar=/usr/bin/tar

FILE=$1
FILEBASE=`/usr/bin/basename ${FILE}`
WORKING_DIR="/var/tmp"
TARGET_DIR="/usr/local/testvectors"
TARGET_DIR="/tmp"
#TARGET_DIR="."

URL_BASE="http://tirgo.arcetri.astro.it/testvectors/"
#WGET_OPTS='--user="jim" --password="xxx-yyy-zzz"'
WGET_OPTS='-c '

if [ ! -x "$wget" ]; then
    echo "ERROR: No wget." >&2
    exit 1
elif [ ! -x "$tar" ]; then
    echo "ERROR: No tar" >&2
    exit 1
fi

if [ $# -lt 1 ]; then
    echo $0 ': usage: get_testvectors filename [savedir]' >&2
    echo 'default savedir /tmp ' >&2
    echo 'suggested test /zerovectors/zero16m.dat' >&2
    echo 'available vectors:' >&2
    FILE="/zerovectors/zero16m.dat"
    FILE="dat.list"
    FILEBASE=`/usr/bin/basename ${FILE}`
    $wget $WGET_OPTS --output-document=- -q ${URL_BASE}/${FILE}
    exit 0
fi

if [ $# -eq 2 ]; then
    TARGET_DIR = $2
else
    echo $0': usage: get_testvectors filename [savedir]' >&2
    exit 0
fi

# change to target directory
if ! cd "$TARGET_DIR"; then
    echo "ERROR: can't access working directory ($TARGET_DIR)" >&2
    exit 1
fi

# wget to download md5sum file
if ! $wget $WGET_OPTS ${URL_BASE}/${FILE}.txt; then
    echo "ERROR: can't get md5sum" >&2
    exit 2
fi

# wget to download file
if ! $wget $WGET_OPTS ${URL_BASE}/${FILE}; then
    echo "ERROR: can't get file" >&2
    exit 3
fi


# check md5sum
if  ! $md5sum -c ${FILEBASE}.txt; then
    echo "ERROR: failed md5sum check" >&2
    exit 5
fi
