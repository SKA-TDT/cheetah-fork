@page fldo_algorithm_guide FLDO algorithm guide
# Notes for developers
## FLDO algorithm
FLDO consists of folding and optimisaton of data for given candidate parameters. The folding algorithm "folds" input TimeFrequency data (i.e dynamic spectra) given a specific pulse period, dispersion measure (DM), and period derivative (Pdot) for each candidate, resulting in a pulse profile for the candidate. The nomenclature of "folding" comes from the idea of folding the data over on itself modulo the pulse period. In actuality, it's more correct to think of the data being chopped up into pulse-period length chunks which are then all added together (as folding suggests a reversal in time order, which does not happen).

The measured period derivative is the addition of the intrinsic period derivative of the pulsar (likely to be negligible over the length of an observation) and any line-of-sight acceleration the pulsar is experiencing due to orbital motion during an observation (which depends on where the pulsar is in its orbit).

### Folding
In general, during folding, the data are segmented into subbands (subsets of frequency channels, with equal numbers of channels per subband comprising the entire bandwidth) and sub-integrations (or 'subints'; subsets of time samples). The samples in each data chunk are corrected for dispersion (dedispersed), then added together modulo the candidate period; the data are binned in time across the candidate period, resulting in a phase between 0 and 1.

The output of this process is a data cube of L subbands x M subints x N phase bins. Note that this process reduces the time resolution of the data to the size of a subint and the frequency resolution to the size of a subband.

### Optimisation
Following folding, an optimisation step occurs. The search parameters that lead to the detection of a candidate are generally sampled such that they cover the required period/DM/Pdot space in a reasonable number of steps. This means that the reported candidate parameters might be different from the true parameters of the pulsar. Therefore, a finer search over period/DM/Pdot is performed. This can be achieved by applying small shifts to the phase of the data in the subbands and subints. Comparing the signal-to-noise of the profile after each trial should lead to a maximum value at the true parameters for the candidate
