/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace altera {

#ifdef SKA_CHEETAH_ENABLE_OPENCL
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& fpga,
    data::TimeSeries<cheetah::Fpga,T,InputAlloc> const& input,
    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output) const
{
    // update the size of the output buffer to match output transform size, only half spectrum considered
    output.resize(input.size()/2 + 1);

    // call FFT implementation for a device worker object
    _workers(fpga)(input, output);

    // Calculate the new frequency step that the output will have
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
}

/*
 * commented FFT types for future scope: C2R, C2C_fwd, C2C_inv
 */
/*
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& fpga,
    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, InputAlloc> const& input,
    data::TimeSeries<cheetah::Fpga,T,OutputAlloc>& output)
{
    //update the size of the output buffer to match output transform size
    output.resize(2*(input.size() - 1));
    //Calculate the new sampling time that the output will have
    output.sampling_interval((1.0f/(input.frequency_step().value()*output.size())) * data::seconds);
}
*/


template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& fpga,
    data::TimeSeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input,
    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output) const
{
    // update the size of the output buffer to match output transform size
    output.resize(input.size());

    // call FFT implementation for a device worker object
    _workers(fpga)(input, output);

    // Calculate the new frequency step that the output will have
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
}

template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& fpga,
    data::FrequencySeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input,
    data::TimeSeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output) const
{
    // update the size of the output buffer to match output transform size
    output.resize(input.size());

    // call FFT implementation for a device worker object
    _workers(fpga)(input, output);

    // Calculate the new sampling time that the output will have
    output.sampling_interval((1.0f/(input.frequency_step().value()*output.size())) * data::seconds);
}

// Plan for time -> frequency transform
template <typename T1, typename T2, typename ArchT1
         , typename ArchT2, typename InputAllocT, typename OutputAllocT
         >
fft::Plan Fft::plan(data::TimeSeries<ArchT1, T1, InputAllocT> const& input
              , panda::TypeTag<data::FrequencySeries<ArchT2, T2, OutputAllocT>> const&) const
{
    std::size_t fft_size = this->calculate_fft_size<T1>(input.size());
    return Plan(fft_size);
}

// Plan for frequency -> time transform
template <typename T1, typename T2, typename ArchT1
         , typename ArchT2, typename InputAllocT, typename OutputAllocT
         >
fft::Plan Fft::plan(data::FrequencySeries<ArchT1, T1, InputAllocT> const& input
              , panda::TypeTag<data::TimeSeries<ArchT2, T2, OutputAllocT>> const&) const
{
    std::size_t fft_size = this->calculate_fft_size<T1>(input.size());
    return Plan(fft_size);
}

/**
 * @details FftSizeHelper struct handles different truncation cases based on R2C or C2C type
*/
namespace
{
    // generic struct, compiled when the input data is real
    template<typename T, typename Enable=void>
    struct FftSizeHelper
    {
        // Truncation to relevant lower power of 2. Altera R2C can handle data size with odd power of 2
        static inline void exec(std::size_t input_power_two, std::size_t& input_size, std::size_t& fft_size)
        {
            // data size around even power of 2
            if (input_power_two % 2 == 0) {
                --input_power_two;
                fft_size = pow (2, input_power_two);
            }
            // data size around odd power of 2 but exceeds _max_padding
            else if (input_power_two % 2 == 1 && input_size < fft_size) {
                input_power_two -= 2;
                fft_size = pow (2, input_power_two);
            }
        }
    };

    // specialized struct, compiled when the input data is complex
    template<typename T>
    struct FftSizeHelper<T, data::EnableIfIsComplexNumber<T>>
    {
        // Truncation to relevant lower power of 2. Altera C2C can handle data size with even power of 2
        static inline void exec(std::size_t input_power_two, std::size_t& input_size, std::size_t& fft_size)
        {
            // data size around odd power of 2
            if (input_power_two % 2 == 1) {
                --input_power_two;
                fft_size = pow (2, input_power_two);
            }
            // data size around even power of 2 but exceeds _max_padding
            else if (input_power_two % 2 == 0 && input_size < fft_size) {
                input_power_two -= 2;
                fft_size = pow (2, input_power_two);
            }
        }
    };
} // namespace

template<typename T>
std::size_t Fft::calculate_fft_size(std::size_t input_size) const
{
    std::size_t input_power_two = round(log2((double)input_size));
    std::size_t fft_size = pow (2, input_power_two);
    std::size_t samples_diff = fft_size - input_size;

    // Truncate if sample difference exceeds _max_padding defined
    if (samples_diff > _max_padding)
        FftSizeHelper<T>::exec(input_power_two, input_size, fft_size);

    return fft_size;
}

#endif // SKA_CHEETAH_ENABLE_OPENCL

} // namespace altera
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
