/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fft/altera/Fft.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace altera {

// TBD: A design parameter to be set, maximum zero padding that can be applied safely.
const std::size_t Fft::_max_padding = 10;

Fft::WorkerFactory::WorkerFactory()
{
}

altera::FftWorker* Fft::WorkerFactory::operator()(panda::PoolResource<Architecture> const& device)
{
    return new altera::FftWorker(device);
}

Fft::Fft(Config const&)
    : _workers(WorkerFactory())
{
}

Fft::Fft(Fft&& other)
    : _workers(std::move(other._workers))
{
}

} // namespace altera
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
