include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_fft_altera_src
    src/FftTest.cpp
    src/gtest_altera.cpp
)

add_executable(gtest_fft_altera ${gtest_fft_altera_src})
target_link_libraries(gtest_fft_altera ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_fft_altera gtest_fft_altera --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
