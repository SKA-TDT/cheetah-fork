/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_CONFIG_H
#define SKA_CHEETAH_MODULES_FFT_CONFIG_H

#include "cheetah/modules/fft/detail/FftTraits.h"
#include "cheetah/modules/fft/detail/FftAlgos.h"
#include "cheetah/utils/Config.h"
#include "cheetah/utils/NullHandler.h"

#include "panda/PoolSelector.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {


/**
 * @brief
 *    Configuration for the fft module
 *
 */
class Config : public utils::Config
{
        typedef utils::Config BaseT;
        typedef FftTraits<float, utils::NullHandler> TraitsT;
        typedef typename TimeR2CTraits<TraitsT, FftAlgos<TraitsT>>::ConfigType TimeR2CConfig;
        typedef typename TimeC2CTraits<TraitsT, FftAlgos<TraitsT>>::ConfigType TimeC2CConfig;
        typedef typename FreqC2CTraits<TraitsT, FftAlgos<TraitsT>>::ConfigType FreqC2CConfig;
        typedef typename FreqC2RTraits<TraitsT, FftAlgos<TraitsT>>::ConfigType FreqC2RConfig;

    public: // methods
        Config();

        // Each method in the fft interface has its own associated configuration
        /**
         * @brief      Get the configuration of the real TimeSeries -> complex  FrequencySeries Transform
         */
        TimeR2CConfig const& time_real_to_complex() const;
        TimeR2CConfig& time_real_to_complex();

        /**
         * @brief      Get the configuration of the complex TimeSeries -> complex FrequencySeries Transform
         */
        TimeC2CConfig const& time_complex_to_complex() const;
        TimeC2CConfig& time_complex_to_complex();

        /**
         * @brief      Get the configuration of the complex FrequencySeries -> complex TimeSeries Transform
         */
        FreqC2CConfig const& freq_complex_to_complex() const;
        FreqC2CConfig& freq_complex_to_complex();

        /**
         * @brief      Get the configuration of the complex FrequencySeries -> real TimeSeries Transform
         */
        FreqC2RConfig const& freq_complex_to_real() const;
        FreqC2RConfig& freq_complex_to_real();

        /**
         * @brief set the configuration for all algorithms to the inactive state
         */
        void deactivate_all();

        /**
         * @brief set the configuration for the specified algorithm to active for all supported fft interfaces
         */
        template<class AlgoT>
        void activate();

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        TimeR2CConfig _time_real_to_complex;
        TimeC2CConfig _time_complex_to_complex;
        FreqC2CConfig _freq_complex_to_complex;
        FreqC2RConfig _freq_complex_to_real;
};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fft/detail/Config.cpp"

#endif // SKA_CHEETAH_MODULES_FFT_CONFIG_H
