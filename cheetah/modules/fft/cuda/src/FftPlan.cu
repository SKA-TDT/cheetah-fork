#include "cheetah/modules/fft/cuda/FftPlan.cuh"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace cuda {

bool FftPlan::valid(cufftType cufft_type, std::size_t size, std::size_t batch) const
{
    return ((cufft_type==_cufft_type) && (size==_size) && (batch==_batch));
}


FftPlan::FftPlan()
    : _plan(0)
    , _size(0)
    , _batch(0)
    , _cufft_type(CUFFT_R2C)
{
}

FftPlan::~FftPlan()
{
    destroy_plan();
}

void FftPlan::destroy_plan()
{
    if (_plan)
        CUFFT_ERROR_CHECK(cufftDestroy(_plan));
    _plan = 0;
    _size = 0;
    _batch = 0;
}

template <>
cufftType FftPlan::convert_to_cufft_type<float>(FftType fft_type) const
{
    switch (fft_type)
    {
        case R2C:
        return CUFFT_R2C;
        case C2R:
        return CUFFT_C2R;
        case C2C:
        return CUFFT_C2C;
        default:
        return CUFFT_R2C; // this should throw but no return value raises warning
    }
}

template <>
cufftType FftPlan::convert_to_cufft_type<double>(FftType fft_type) const
{
    switch (fft_type)
    {
        case R2C:
        return CUFFT_D2Z;
        case C2R:
        return CUFFT_Z2D;
        case C2C:
        return CUFFT_Z2Z;
        default:
        return CUFFT_D2Z; // this should throw but no return value raises warning
    }
}

} // namespace cuda
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
