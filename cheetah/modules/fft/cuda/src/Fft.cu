#include "cheetah/modules/fft/cuda/Fft.cuh"
#include "cheetah/modules/fft/detail/AlgoWrapper.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace cuda {

Fft::Fft()
    : _plan(new FftPlan())
{
}

Fft::Fft(Config const&)
    : Fft()
{
}

Fft::Fft(Fft&& other)
    : _plan(std::move(other._plan))
{
}


// explicitly instantitiate a selection of process methods to make available
// in the library to non-nvcc compliers
#define INSTANTIATE_FFT_PROCESS(_ValueT)  \
    namespace cuda { \
    template void Fft::process(typename Fft::ResourceType& \
                     , data::TimeSeries<cheetah::Cuda, _ValueT> const& \
                     , data::FrequencySeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT>>&\
                     ) const; \
    template void Fft::process(typename Fft::ResourceType& \
                 , data::FrequencySeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT>> const& \
                 , data::TimeSeries<cheetah::Cuda, _ValueT>& \
                 ) const; \
    template void Fft::process(typename Fft::ResourceType& \
                     , data::TimeSeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT >> const& \
                     , data::FrequencySeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT>>&\
                     ) const; \
    template void Fft::process(typename Fft::ResourceType& \
                 , data::FrequencySeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT>> const& \
                 , data::TimeSeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda,_ValueT>>& \
                 ) const; \
    } \
    template void fft::AlgoWrapper<cuda::Fft>::operator()(typename Fft::ResourceType& \
                         , fft::CommonPlan const& \
                         , data::TimeSeries<cheetah::Cuda, _ValueT> const& \
                         , data::FrequencySeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT>>&\
                     ) const; \
    template void fft::AlgoWrapper<cuda::Fft>::operator()(typename Fft::ResourceType& \
                         , fft::CommonPlan const& \
                         , data::FrequencySeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT>> const& \
                         , data::TimeSeries<cheetah::Cuda, _ValueT>& \
                     ) const; \
    template void fft::AlgoWrapper<cuda::Fft>::operator()(typename Fft::ResourceType& \
                         , fft::CommonPlan const& \
                         , data::TimeSeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT >> const& \
                         , data::FrequencySeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT>>&\
                     ) const; \
    template void fft::AlgoWrapper<cuda::Fft>::operator()(typename Fft::ResourceType& \
                         , fft::CommonPlan const& \
                         , data::FrequencySeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda, _ValueT>> const& \
                         , data::TimeSeries<cheetah::Cuda, data::ComplexTypeTraits_t<cheetah::Cuda,_ValueT>>& \
                     ) const


} // namespace cuda
INSTANTIATE_FFT_PROCESS(double);
INSTANTIATE_FFT_PROCESS(float);
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
