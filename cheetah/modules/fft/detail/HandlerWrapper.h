/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_HANDLERWRAPPER_H
#define SKA_CHEETAH_MODULES_FFT_HANDLERWRAPPER_H

#include "cheetah/modules/fft/detail/FftDataTypeTraits.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @brief forwards data of the appropriate type to the Fft algorithm results Handler
 * @details Will convert the data to a CPU host memory version.
 *          TODO  It is intended to only do this where the handler
 *          cannot accept the device memory version of the data
 */

template<typename FftTraitsT>
class HandlerWrapper
{
    public:
        typedef typename FftTraitsT::Handler Handler;

    public:
        HandlerWrapper(Handler& handler);

        template<typename DataT>
        void operator()(std::shared_ptr<DataT> data) const;

    protected:
        template<typename DataT
               , EnableIfHasArch<panda::Cpu, DataT, bool> = true
               >
        void exec_handler(std::shared_ptr<DataT>) const;

        template<typename DataT
               , EnableIfNotHasArch<panda::Cpu, DataT, bool> = true
               >
        void exec_handler(std::shared_ptr<DataT>) const;

    private:
        Handler _handler;
};

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fft/detail/HandlerWrapper.cpp"

#endif // SKA_CHEETAH_MODULES_FFT_HANDLERWRAPPER_H
