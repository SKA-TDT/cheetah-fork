/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_FFTALGOS_H
#define SKA_CHEETAH_MODULES_FFT_FFTALGOS_H

#include "cheetah/Configuration.h"
#include "cheetah/modules/fft/cuda/Fft.h"
#include "cheetah/modules/fft/altera/Fft.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @brief Define the algos that are available depending on the configuration settings
 * @details If you add a new algorithm that supports the interface then add it into
 *          this tuple and it will automatically be integrated into the top level Fft
 *          interface.
 *          Note that the appropriate SKA_CHEETAH_ENABLE_XXXX guards should be added around
 *          any new entry.
 */

template<typename FftTraits>
using FftAlgos = panda::tuple_pop_front_t<std::tuple<int // adding then removing the int resolves
                                                         // any syntax problems with too many ","
#ifdef SKA_CHEETAH_ENABLE_CUDA
                         , cuda::Fft
#endif // SKA_CHEETAH_ENABLE_CUDA
#ifdef SKA_CHEETAH_ENABLE_OPENCL
                         , altera::Fft
#endif // SKA_CHEETAH_ENABLE_OPENCL
                 >>;

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FFT_FFTALGOS_H
