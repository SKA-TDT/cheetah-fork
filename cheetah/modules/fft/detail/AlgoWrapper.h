/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_ALGOWRAPPER_H
#define SKA_CHEETAH_MODULES_FFT_ALGOWRAPPER_H

#include "FftDataTypeTraits.h"
#include "CommonPlan.h"
#include <memory>
#include <utility>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @brief Wraps an algorithm so we can apply data conversions as appropriate
 * @details Input data fed to the active Fft algorithm will be adapted to the requirements of the provided CommonPlan
 */

template<typename WrappedType>
class AlgoWrapper : public WrappedType
{
        typedef WrappedType BaseT;

    public:
        template<typename... WrappedTypeConstructorArgs>
        AlgoWrapper(WrappedTypeConstructorArgs&&... args);
        ~AlgoWrapper() = default;

        /**
         * @brief Sync interface for active Fft algos where the output data type matches the Wrapped algorithms Architecture
         */
        template<class ArchT
               , typename InputDataT
               , typename OutputDataT
               , EnableIfHasArch<ArchT, OutputDataT, bool> = true
               >
        void operator()(panda::PoolResource<ArchT>&, CommonPlan const&, InputDataT const&, OutputDataT&) const;

        /**
         * @brief Sync interface for active Fft algos that takes alternative data output types that need converting
         */
        template<class ArchT
               , typename InputDataT
               , typename OutputDataT
               , EnableIfNotHasArch<ArchT, OutputDataT, bool> = true
               >
        void operator()(panda::PoolResource<ArchT>&, CommonPlan const&, InputDataT const&, OutputDataT&) const;

        /**
         * @brief Sync interface for Fft that takes the shared_ptr version of input data
         */
        template<class ArchT, typename InputDataT, typename OutputDataT>
        void operator()(panda::PoolResource<ArchT>&
                      , CommonPlan const&
                      , std::shared_ptr<InputDataT> const&
                      , OutputDataT&) const;

        /**
         * @brief  async interface
         * @return std::shared_ptr<OutputData> where the OutputData will be either a TimeSeries or FrequencySeries class
         *         (depending on the input) and of an Architecture that matches ArchT.
         */
        template<class ArchT
               , typename InputDataT
               , typename OutputT
                >
        std::shared_ptr<FftReplaceArch_t<ArchT, OutputT>> operator()(panda::PoolResource<ArchT>& res
                                          , CommonPlan plan
                                          , std::shared_ptr<InputDataT> const& data
                                          , panda::TypeTag<OutputT> tag) const;

    protected:
        template<class ArchT
               , typename InputDataT
               , typename OutputT
               , EnableIfHasArch<ArchT, OutputT, bool> = true
                >
        std::shared_ptr<OutputT> exec(panda::PoolResource<ArchT>&, CommonPlan
                                    , InputDataT const&
                                    , panda::TypeTag<OutputT>) const;

        template<class ArchT
               , typename InputDataT
               , typename OutputT
               , EnableIfNotHasArch<ArchT, OutputT, bool> = true
                >
        std::shared_ptr<FftReplaceArch_t<ArchT, OutputT>> exec(panda::PoolResource<ArchT>&
                                    , CommonPlan
                                    , InputDataT const&
                                    , panda::TypeTag<OutputT>) const;

        template<class ArchT
               , typename InputDataT
               , typename OutputDataT
               , EnableIfHasArch<ArchT, InputDataT, bool> = true
                >
        void launch_algo(panda::PoolResource<ArchT>& device, InputDataT const& input_data, OutputDataT& output_data) const;

        template<class ArchT
               , typename InputDataT
               , typename OutputDataT
               , EnableIfNotHasArch<ArchT, InputDataT, bool> = true
                >
        void launch_algo(panda::PoolResource<ArchT>& device, InputDataT const& input_data, OutputDataT& output_data) const;

        template<class ArchT
               , typename InputDataT
               , typename OutputDataT
               , EnableIfHasArch<ArchT, InputDataT, bool> = true
                >
        void shrink_and_launch_algo(panda::PoolResource<ArchT>& device
                                  , InputDataT const& input_data
                                  , OutputDataT& output_data
                                  , std::size_t new_size) const;

        template<class ArchT
               , typename InputDataT
               , typename OutputDataT
               , EnableIfNotHasArch<ArchT, InputDataT, bool> = true
                >
        void shrink_and_launch_algo(panda::PoolResource<ArchT>& device
                                  , InputDataT const& input_data
                                  , OutputDataT& output_data
                                  , std::size_t new_size) const;

        template<class ArchT
               , typename InputDataT
               , typename OutputDataT
               , EnableIfHasArch<ArchT, InputDataT, bool> = true
                >
        void pad_and_launch_algo(panda::PoolResource<ArchT>& device
                               , InputDataT const& input_data
                               , OutputDataT& output_data
                               , std::size_t new_size) const;

        template<class ArchT
               , typename InputDataT
               , typename OutputDataT
               , EnableIfNotHasArch<ArchT, InputDataT, bool> = true
                >
        void pad_and_launch_algo(panda::PoolResource<ArchT>& device
                               , InputDataT const& input_data
                               , OutputDataT& output_data
                               , std::size_t new_size) const;

};


} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FFT_ALGOWRAPPER_H
#include "AlgoWrapper.cpp"
