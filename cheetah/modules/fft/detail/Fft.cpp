/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fft/Fft.h"
#include "panda/TupleUtilities.h"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <utility>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

template<typename Handler, typename NumericalT>
Fft<Handler, NumericalT>::Fft(fft::ConfigType const& config, Handler& handler)
    : BaseT(config, handler)
    , _config(config)
{
}

/**
 *
 * Forward the FFT request to the relevant method of the first implementation that will accept it.
 * The algorithm is determined via an Architecture : Implementation map which must be specified in
 * the scope of the containing class. This provides compile-time resolution of the call chain.
 *
 */
template<typename Handler, typename NumericalT>
template <typename Arch, typename InputType, typename OutputType, typename... Args>
void Fft<Handler, NumericalT>::process(panda::PoolResource<Arch>& resource
                                      , InputType const& input
                                      , OutputType& output
                                      , Args&&... args)
{
    BaseT::exec(resource, std::forward<InputType const&>(input), output, std::forward<Args>(args)...);
}

template<typename Handler, typename NumericalT>
template<class InputArch, typename InputType>
std::shared_ptr<panda::ResourceJob> Fft<Handler, NumericalT>::fft_to_complex(std::shared_ptr<data::TimeSeries<InputArch, InputType>> const& input) const
{
    return BaseT::template submit<InputArch>(input);
}

template<typename Handler, typename NumericalT>
template<class InputArch, typename InputType>
std::shared_ptr<panda::ResourceJob> Fft<Handler, NumericalT>::fft_to_complex(std::shared_ptr<data::FrequencySeries<InputArch, InputType>> const& input) const
{
    return BaseT::template submit<InputArch, InputArch, InputType>(input);
}

template<typename Handler, typename NumericalT>
template<typename InputType>
std::shared_ptr<panda::ResourceJob> Fft<Handler, NumericalT>::fft_to_real(std::shared_ptr<InputType> const& input) const
{
    return this->template submit_to_real<typename InputType::Architecture>(input);
}

template<typename Handler, typename NumericalT>
template <typename InputTypeHost>
void Fft<Handler, NumericalT>::normalize_about_zero(InputTypeHost& host_input)
{
    normalize_about_zero(host_input.begin(), host_input.end());
}

template<typename Handler, typename NumericalT>
template <typename InputIt>
void Fft<Handler, NumericalT>::normalize_about_zero(InputIt&& begin, InputIt const& end)
{
    typedef typename std::iterator_traits<InputIt>::value_type T;
    normalize_about_zero_implementation<T>(std::forward<InputIt>(begin), end);
}

template<typename Handler, typename NumericalT>
template <typename T, typename InputIt, typename std::enable_if<std::is_unsigned<T>::value, bool>::type>
void Fft<Handler, NumericalT>::normalize_about_zero_implementation(InputIt, InputIt const&)
{
    PANDA_LOG_WARN << "skipped Chi-sq normalization for the unsigned data";
}

template<typename Handler, typename NumericalT>
template <typename T, typename InputIt, typename std::enable_if<!std::is_unsigned<T>::value, bool>::type>
void Fft<Handler, NumericalT>::normalize_about_zero_implementation(InputIt begin, InputIt const& end)
{
    boost::accumulators::accumulator_set<T
                                        , boost::accumulators::features<boost::accumulators::tag::mean
                                                                      , boost::accumulators::tag::variance
                                                                       >
                                        > acc;
    InputIt begin_copy = begin;
    for (auto it = begin; it != end; ++it) {
        acc(*it);
    }
    T mean = boost::accumulators::mean(acc);
    T std_dev = sqrt(boost::accumulators::variance(acc));
    for (auto it = begin_copy; it != end; ++it) {
        *it = *it - mean;
        *it = *it / std_dev;
    }

}

template<typename Handler, typename NumericalT>
template<typename OutputTypeHost>
void Fft<Handler, NumericalT>::normalize_sqrt_two_by_length(OutputTypeHost& host_output, std::size_t fft_size)
{
    normalize_sqrt_two_by_length(host_output.begin(), host_output.end(), fft_size);
}

template<typename Handler, typename NumericalT>
template<typename OutputIt>
void Fft<Handler, NumericalT>::normalize_sqrt_two_by_length(OutputIt begin, OutputIt const& end, std::size_t fft_size)
{
    typedef typename std::iterator_traits<OutputIt>::value_type T;
    T norm_factor = sqrt(2.0 / fft_size);
    for (auto it = begin; it != end; ++it) {
        *it = (*it) * norm_factor;
    }
}

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
