/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fft/Config.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

Config::Config()
    : BaseT("fft")
    , _time_real_to_complex("time_real_to_complex")
    , _time_complex_to_complex("time_complex_to_complex")
    , _freq_complex_to_complex("freq_complex_to_complex")
    , _freq_complex_to_real("freq_complex_to_real")
{
    add(_time_real_to_complex);
    add(_time_complex_to_complex);
    add(_freq_complex_to_complex);
    add(_freq_complex_to_real);
}

void Config::add_options(OptionsDescriptionEasyInit&)
{
}

void Config::deactivate_all()
{
    _time_real_to_complex.deactivate_all();
    _time_complex_to_complex.deactivate_all();
    _freq_complex_to_complex.deactivate_all();
    _freq_complex_to_real.deactivate_all();
}

Config::TimeR2CConfig const& Config::time_real_to_complex() const
{
    return _time_real_to_complex;
}

Config::TimeR2CConfig& Config::time_real_to_complex()
{
    return _time_real_to_complex;
}

Config::TimeC2CConfig const& Config::time_complex_to_complex() const
{
    return _time_complex_to_complex;
}

Config::TimeC2CConfig& Config::time_complex_to_complex()
{
    return _time_complex_to_complex;
}

Config::FreqC2CConfig const& Config::freq_complex_to_complex() const
{
    return _freq_complex_to_complex;
}

Config::FreqC2CConfig& Config::freq_complex_to_complex()
{
    return _freq_complex_to_complex;
}

Config::FreqC2RConfig const& Config::freq_complex_to_real() const
{
    return _freq_complex_to_real;
}

Config::FreqC2RConfig& Config::freq_complex_to_real()
{
    return _freq_complex_to_real;
}

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
