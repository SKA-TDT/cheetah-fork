/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fft/test_utils/FftTester.h"
#include "cheetah/modules/fft/detail/HasFftProcessMethod.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/utils/System.h"
#include "panda/Log.h"
#include "panda/TypeTraits.h"
#include "panda/PoolManager.h"
#include "panda/PoolManagerConfig.h"

#include <boost/math/constants/constants.hpp>
#include <functional>
#include <cmath>
#include <random>


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace test {

namespace {

    // Create noise data with specific normal distribution
    template<typename DataT>
    void fill_with_random_data(DataT& data, float mean_n, float std_dev_n)
    {
        typedef typename std::iterator_traits<decltype(data.begin())>::value_type T;
        std::random_device rd;
        std::mt19937 gen(rd());
        std::normal_distribution<T> noise_data(mean_n, std_dev_n);
        std::generate(data.begin(), data.end(), [&]()
                                                {
                                                    return noise_data(gen);
                                                }
                     );
    }

    // Calculate Mean value using Boost accumulators library
    template<typename DataT, typename T>
    T mean(DataT& data)
    {
        boost::accumulators::accumulator_set<T
                                            , boost::accumulators::features<boost::accumulators::tag::mean>
                                            > acc;
        for(auto& val : data) {
            acc(val);
        }
        return (T)boost::accumulators::mean(acc);
    }

    // Calculate standard deviation using Boost accumulators library
    template<typename DataT, typename T>
    T standard_deviation(DataT& data)
    {
        boost::accumulators::accumulator_set<T
                                            , boost::accumulators::features<boost::accumulators::tag::variance>
                                            > acc;
        for(auto& val : data) {
            acc(val);
        }
        return (T)sqrt(boost::accumulators::variance(acc));
    }

    template<typename TypeParam, typename InputValueT, typename OutputValueT>
    using HasTime2Frequency = fft::HasTime2Frequency<typename TypeParam::Algo, InputValueT, OutputValueT>;

    template<typename TypeParam, typename InputValueT, typename OutputValueT>
    using HasFrequency2Time = fft::HasFrequency2Time<typename TypeParam::Algo, InputValueT, OutputValueT>;

    template<typename TypeParam, class InputType, class OutputType
           , typename Enable=void>
    struct ProcessHelper
    {
        static inline
        bool exec(TypeParam& traits
                , typename TypeParam::DeviceType& device
                , InputType const& input
                , OutputType& output
                )
        {
            traits.api().process(device, input, output);
            return true;
        }
    };

    template<typename TypeParam, class InputType, class OutputType>
    struct ProcessHelper<TypeParam, InputType, OutputType
                        , typename std::enable_if<
                                       !HasFftProcessMethod<typename TypeParam::Algo, InputType, OutputType>::value>::type>
    {
        static inline
        bool exec(TypeParam&
                , typename TypeParam::DeviceType&
                , InputType const&
                , OutputType&
                )
        {
            return false;
        }
    };

    /*
     * @brief  attempts to invoke the process() method on the API
     * @details if method exists it will be executed
     * @returns true : if process method exists
     *          false: otherwise
     */
    template<typename TypeParam, class InputType, class OutputType>
    bool process(TypeParam& traits
                , typename TypeParam::DeviceType& device
                , InputType const& input
                , OutputType& output)
    {
        return ProcessHelper<TypeParam, InputType, OutputType>::exec(traits, device, input, output);
    }

} // namespace

// -- Base type for Real to complex type tests
template<class DerivedType, typename TypeParam, typename Enable=void>
struct R2C_Base
{
    static inline
    void exec(typename TypeParam::DeviceType& device, std::size_t fft_trial_length)
    {
        DerivedType::run_test(device, fft_trial_length);
    }

    static inline
    void exec(typename TypeParam::DeviceType& device, std::size_t fft_trial_length, std::size_t cw_bin)
    {
        DerivedType::run_test(device, fft_trial_length, cw_bin);
    }

    static inline
    void exec_async(typename TypeParam::DeviceType& device, std::size_t fft_trial_length, std::size_t cw_bin)
    {
        DerivedType::run_async_test(device, fft_trial_length, cw_bin);
    }

    static inline
    void exec(typename TypeParam::DeviceType& device
             , std::size_t fft_trial_length
             , float mean_n
             , float std_dev_n)
    {
        DerivedType::run_test(device, fft_trial_length, mean_n, std_dev_n);
    }
};

template<class DerivedType, typename TypeParam>
struct R2C_Base<DerivedType, TypeParam
                 , typename std::enable_if<
                                 !HasTime2Frequency<TypeParam
                                                   , typename TypeParam::NumericalRep
                                                   , typename TypeParam::ComplexT
                                                   >::value
                                          >::type>
{
    static inline
    void exec(typename TypeParam::DeviceType&
             , std::size_t
             , float
             , float)
    {
        std::cout << "Real to Complex FT not defined for this device\n";
    }

    static inline
    void exec_async(typename TypeParam::DeviceType&
                   , std::size_t
                   , std::size_t)
    {
        std::cout << "Real to Complex FT not defined for this device\n";
    }
};

// -- Base type for complex to complex type tests
template<class DerivedType, typename TypeParam, typename Enable=void>
struct C2C_Base
{
    static inline
    void exec(typename TypeParam::DeviceType& device, std::size_t fft_trial_length)
    {
        DerivedType::run_test(device, fft_trial_length);
    }

    static inline
    void exec(typename TypeParam::DeviceType& device, std::size_t fft_trial_length, std::size_t cw_bin)
    {
        DerivedType::run_test(device, fft_trial_length, cw_bin);
    }

    static inline
    void exec_async(std::size_t size)
    {
        DerivedType::run_async_test(size);
    }
};

template<class DerivedType, typename TypeParam>
struct C2C_Base<DerivedType, TypeParam
                 , typename std::enable_if<
                                 !HasTime2Frequency<TypeParam
                                                   , typename TypeParam::ComplexT
                                                   , typename TypeParam::ComplexT
                                                   >::value
                                          >::type>
{
    static inline
    void exec(typename TypeParam::DeviceType&)
    {
        std::cout << "Complex to Complex FT not defined for this device\n";
    }

    static inline
    void exec_async(std::size_t)
    {
        std::cout << "Complex to Complex FT not defined for this device\n";
    }
};

template<typename Algorithm, typename NumericalT>
FftTesterTraits<Algorithm, NumericalT>::FftTesterTraits()
    : _pool_manager(utils::system(), _pool_manager_config)
{
}

template<typename Algorithm, typename NumericalT>
typename FftTesterTraits<Algorithm, NumericalT>::FftApi& FftTesterTraits<Algorithm, NumericalT>::api()
{
    if(!_api) {
        typedef typename utils::Config::SystemType System;
        static panda::PoolManagerConfig<System> pool_manager_config;
        static panda::PoolManager<System> pool_manager(utils::system(), pool_manager_config);
        _config.reset(new Config(_pool_manager));
        _config->deactivate_all();
        _config->template activate<Algo>();
        _api.reset(new FftApi(*_config, _handler));
    }
    return *_api;
}

template<typename Algorithm, typename NumericalT>
typename FftTesterTraits<Algorithm, NumericalT>::FftHandler const& FftTesterTraits<Algorithm, NumericalT>::handler() const
{
    return _handler;
}

template <typename TestTraits>
FftTester<TestTraits>::FftTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
FftTester<TestTraits>::~FftTester()
{
}

template<typename TestTraits>
void FftTester<TestTraits>::SetUp()
{

}

template<typename TestTraits>
void FftTester<TestTraits>::TearDown()
{
}

template<typename TypeParam>
struct FftR2C_ZeroTest : public R2C_Base<FftR2C_ZeroTest<TypeParam>, TypeParam>
{
    static inline
    void run_test(typename TypeParam::DeviceType& device, std::size_t fft_trial_length)
    {
        TypeParam traits;
        typedef typename TypeParam::NumericalRep T;
        typedef typename TypeParam::Arch Arch;
        typedef typename TypeParam::ComplexT ComplexT;

        // create device and host buffer types for given and complex data types
        typedef data::TimeSeries<Arch, T> InputType;
        typedef data::FrequencySeries<Arch, ComplexT> OutputType;
        typedef data::TimeSeries<Cpu, T> InputDataCpu;
        typedef data::FrequencySeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> OutputTypeHost;

        // create TimeSeries buffer with sampling period 0.003 s, this can be any value depending on sampling
        InputDataCpu data_in(data::TimeType(0.003 * data::seconds), fft_trial_length);
        std::fill(data_in.begin(), data_in.end(), 0);

        // allocate device structures
        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));

        // FFT process call
        traits.api().process(device, input, output);

        // check output metadata
        ASSERT_EQ(output.size(), input.size()/2 + 1);
        auto freq_step_exp = (1.0f/(input.sampling_interval().value() * input.size()));
        ASSERT_EQ(output.frequency_step().value(), freq_step_exp);

        // copy output data from device to host buffer
        OutputTypeHost data_out(output);

        // verify FFT output
        for(auto const& elm : data_out){
            ASSERT_FLOAT_EQ(elm.real(), 0.0);
            ASSERT_FLOAT_EQ(elm.imag(), 0.0);
        }

        // define device buffer for inverse transform
        InputType inverse(traits.template allocator<InputType>(device));

        // perform inverse transform if it is defined for the algorithm
        if(! process(traits, device, output, inverse))
        {
            std::cout << "Inverse transform not availaable - skipping test\n";
            return;
        }

        // copy inverse transform data from device to host buffer and verify it's size
        InputDataCpu inverse_host(inverse);
        ASSERT_EQ(inverse_host.size(), data_in.size());

        auto inv_it = inverse_host.begin();
        std::size_t count = 0;
        for(auto const& elm : data_in)
        {
            SCOPED_TRACE(std::to_string(count));

            // assert the inverse with original data
            ASSERT_FLOAT_EQ(elm, *inv_it);

            ++inv_it;
            ++count;
        }
    }
};

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_zero_128)
{
    FftR2C_ZeroTest<TypeParam>::exec(device, 128);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_zero_2048)
{
    FftR2C_ZeroTest<TypeParam>::exec(device, 2048);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_zero_8388608)
{
    FftR2C_ZeroTest<TypeParam>::exec(device, 8388608);
}

/**
 * @details
 * insert a delta fn off size equal to the number of elements in the first position
 * we expect the impulse of magnitude equal to the size f this pulse and all in the real place
 */
template<typename TypeParam>
struct FftR2C_DeltaTest : public R2C_Base<FftR2C_DeltaTest<TypeParam>, TypeParam>
{
    static inline
    void run_test(typename TypeParam::DeviceType& device, std::size_t fft_trial_length)
    {
        TypeParam traits;
        typedef typename TypeParam::NumericalRep T;
        typedef typename TypeParam::Arch Arch;
        typedef typename TypeParam::ComplexT ComplexT;

        // create device and host buffer types for given and complex data types
        typedef data::TimeSeries<Arch, T> InputType;
        typedef data::FrequencySeries<Arch, ComplexT> OutputType;
        typedef data::TimeSeries<Cpu, T> InputDataCpu;
        typedef data::FrequencySeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> OutputTypeHost;

        // create TimeSeries buffer with sampling period 0.003 s, this can be any value depending on sampling
        auto data_time = data::TimeType(0.003 * data::seconds);
        InputDataCpu data_in(data_time, fft_trial_length);

        // fill up input buffer with shifted delta function in time domain
        *(data_in.begin()) = 1;
        std::fill(data_in.begin() + 1, data_in.end(), 0);

        // allocate host data on the device buffers using std::allocator_traits
        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));

        // FFT process call
        traits.api().process(device, input, output);

        // check FFT output metadata
        ASSERT_EQ(output.size(), input.size() / 2 + 1);
        auto freq_step_exp = (1.0f/(input.sampling_interval().value() * input.size()));
        ASSERT_EQ(output.frequency_step().value(), freq_step_exp);

        // copy FFT output data from device to host buffer
        OutputTypeHost data_out(output);

        // verify FFT of delta function, i.e. constant magnitude for all real parts and imaginary parts to zeros
        std::size_t count = 0;
        for(auto const& elm : data_out){
            SCOPED_TRACE(std::to_string(count));

            // n.b. output types are not normalised by default
            ASSERT_FLOAT_EQ(round(elm.real()), 1);
            ASSERT_TRUE(std::abs(elm.real() - 1.0) < TypeParam::accuracy());

            ASSERT_FLOAT_EQ(round(elm.imag()), 0.0);
            ASSERT_TRUE(std::abs(elm.imag() - 0.0) < TypeParam::accuracy());

            ++count;
        }

        // perform inverse transform (i.e a c2r transform)
        InputType inverse(traits.template allocator<InputType>(device));
        if(! process(traits, device, output, inverse))
        {
            std::cout << "Inverse transform not availaable - skipping test\n";
            return;
        }

        // check metadata of inverse transform
        ASSERT_EQ(inverse.size(), 2*(output.size() - 1));
        auto sampling_step_exp = (1.0f/(output.frequency_step().value() * inverse.size()));
        ASSERT_EQ(inverse.sampling_interval().value(), sampling_step_exp);

        // copy inverse transform data from device to host buffer and verify it's size
        InputDataCpu inverse_host(inverse);
        ASSERT_EQ(inverse_host.size(), data_in.size());

        auto inv_it = inverse_host.begin();
        std::size_t count_inv = 0;
        for(auto const& elm : data_in)
        {
            SCOPED_TRACE(std::to_string(count_inv));

            // verfiy inverse has returned the original data for a defined accuracy
            ASSERT_TRUE(std::abs(elm - (*inv_it)/output.size()) < TypeParam::accuracy())
                       << "expected:" << elm*output.size() << " got:" << *inv_it
                       << " accuracy:" << TypeParam::accuracy();

            ++inv_it;
            ++count_inv;
        }
    }
};

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_c2r_delta_128)
{
    FftR2C_DeltaTest<TypeParam>::exec(device, 128);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_c2r_delta_2048)
{
    FftR2C_DeltaTest<TypeParam>::exec(device, 2048);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_c2r_delta_8388608)
{
    FftR2C_DeltaTest<TypeParam>::exec(device, 8388608);
}




/**
 * @details
 * insert a delta fn of size equal to the number of elements at a shifted position
 * we expect the impulse of magnitude equal to the size f this pulse
 */
template<typename TypeParam>
struct FftR2C_ShiftTest : public R2C_Base<FftR2C_ShiftTest<TypeParam>, TypeParam>
{
    typedef typename TypeParam::NumericalRep T;
    typedef typename TypeParam::Arch Arch;
    typedef typename TypeParam::ComplexT ComplexT;

    typedef data::TimeSeries<Arch, T> InputType;
    typedef data::FrequencySeries<Arch, ComplexT> OutputType;
    typedef data::TimeSeries<Cpu, T> InputDataCpu;
    typedef data::FrequencySeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> OutputTypeHost;
    typedef typename TypeParam::DeviceType Device;

    static inline
    void run_test(typename TypeParam::DeviceType& device, std::size_t fft_trial_length)
    {
        TypeParam traits;

        // create TimeSeries buffer with sampling period 0.003 s, this can be any value depending on sampling
        auto data_time = data::TimeType(0.003 * data::seconds);
        InputDataCpu data_in(data_time, fft_trial_length);

        // fill up input buffer with shifted delta function in time domain
        *(data_in.begin()) = 0;
        *(data_in.begin() + 1) = 1;
        std::fill(data_in.begin() + 3, data_in.end(), 0);

        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));

        // FFT process call
        traits.api().process(device, input, output);

        // check output metadata
        ASSERT_EQ(output.size(), input.size() / 2 + 1);
        auto freq_step_exp = (1.0f/(input.sampling_interval().value() * input.size()));
        ASSERT_EQ(output.frequency_step().value(), freq_step_exp);

        // copy output data from device to host buffer
        OutputTypeHost data_out(output);

        // verfiy FFT power has a certain accuracy e.g fwd_accuracy = 1e-6
        T fwd_accuracy = 1e-6;
        size_t count=0;
        for(auto const& elm : data_out){
            SCOPED_TRACE(std::to_string(count));

            // verify magnitude of complex FFT data for a certain accuracy 1e-6
            ASSERT_FLOAT_EQ((T)1, (T)round(sqrt(elm.real()*elm.real() + elm.imag()*elm.imag())));
            ASSERT_TRUE(std::abs((T)(sqrt(elm.real()*elm.real()+elm.imag()*elm.imag())) - (T)1) < fwd_accuracy);

            ++count;
        }
    }
};

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_shift_128)
{
    FftR2C_ShiftTest<TypeParam>::exec(device, 128);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_shift_2048)
{
    FftR2C_ShiftTest<TypeParam>::exec(device, 2048);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_shift_8388608)
{
    FftR2C_ShiftTest<TypeParam>::exec(device, 8388608);
}

/**
 * @details
 * Insert a sine wave where FFT length is integral multiple of bin number of the carrier frequency
 * For a scaled input, we expect the impulse of magnitude unity for given the impulse inserted by cw_bin
 * R2C is defined for odd power of 2 as it uses extra two kernels and C2C transform within implementation
 * C2C is defined for even power of 2 as it gives square matrix to calculate row/column iterations
 * TBD: truncation/zero-padding of input data is not yet included, data length is expected to be exact power of 2
 */
template<typename TypeParam>
struct FftR2C_CwTest : public R2C_Base<FftR2C_CwTest<TypeParam>, TypeParam>
{
    typedef typename TypeParam::NumericalRep T;
    typedef typename TypeParam::Arch Arch;
    typedef typename TypeParam::ComplexT ComplexT;
    typedef typename TypeParam::DeviceType Device;

    typedef data::TimeSeries<Arch, T> InputType;
    typedef data::FrequencySeries<Arch, ComplexT> OutputType;
    typedef data::TimeSeries<Cpu, T> InputDataCpu;
    typedef data::FrequencySeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> OutputTypeHost;

    static
    std::size_t calc_fft_size(std::size_t fft_trial_length)
    {
        return pow (2, round(log2((T)(fft_trial_length)))); // FIXME should use the plan
    }

    static
    std::unique_ptr<InputDataCpu> generate_input_data(std::size_t fft_trial_length, std::size_t cw_bin)
    {
        // create TimeSeries buffer with sampling period 0.003 s, this can be any value depending on sampling
        std::unique_ptr<InputDataCpu> data_in(new InputDataCpu(data::TimeType(0.003 * data::seconds), fft_trial_length));

        size_t cw_samples = calc_fft_size(fft_trial_length)/cw_bin;
        size_t count_input = 0;

        // fill up input buffer with sine wave data, scaled by 2 to get unity impluse after FFT
        for(auto& val : *data_in) {
            val = 2 * sin((2 * (boost::math::constants::pi<T>()) / cw_samples) * count_input);
            ++count_input;
        }

        return data_in;
    }

    template<typename InputT, typename OutputT>
    static
    void verify(InputT const& input, OutputT const& output, std::size_t cw_bin)
    {
        ASSERT_EQ(output.size(), input.size()/2 +1);
        ASSERT_EQ(output.frequency_step().value(), (1.0f/(input.sampling_interval().value() * input.size())));

        // copy data back to host so we can test
        OutputTypeHost data_out(output);

        // verfiy forward transform has calculated FFT with a certain accuracy e.g fwd_accuracy = 1e-4
        T fwd_accuracy = 1e-4;

        std::size_t fft_size = calc_fft_size(input.size());
        size_t count_output = 0;
        auto abs_power_diff = 0.0;
        for(auto const& elm : data_out) {
            SCOPED_TRACE(std::to_string(count_output));

            // calculate and normalize output power by FFT length
            double cw_power = (sqrt(elm.real() * elm.real() + elm.imag() * elm.imag()) / fft_size);

            // carrier wave power is checked for the injected frequency bin, scaled input gives unit output power
            if (count_output == cw_bin) {
                ASSERT_FLOAT_EQ(round(cw_power), (T)1);
            }
            else {
                abs_power_diff = std::abs((T)(cw_power) - (T)0);
                ASSERT_TRUE(abs_power_diff < fwd_accuracy);
            }

            ++count_output;
        }
    }

    static inline
    void run_test(Device& device, std::size_t fft_trial_length, std::size_t cw_bin)
    {
        TypeParam traits;

        auto data_in_ptr = generate_input_data(fft_trial_length, cw_bin);
        InputDataCpu const& data_in = *data_in_ptr;

        // allocate host data on the device buffers using std::allocator_traits
        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));

        // FFT process call
        traits.api().process(device, input, output);
        verify(input, output, cw_bin);

    }

    // asynctest
    static inline
    void run_async_test(Device& device, std::size_t fft_trial_length, std::size_t cw_bin)
    {
        TypeParam traits;

        // generate test data
        auto data_in_ptr = generate_input_data(fft_trial_length, cw_bin);
        InputDataCpu const& data_in = *data_in_ptr;

        // copy to device provided
        std::shared_ptr<InputType> input = std::make_shared<InputType>(data_in, traits.template allocator<InputType>(device));

        auto job = traits.api().fft_to_complex(input);
        job->wait();
        ASSERT_TRUE(traits.handler().wait(std::chrono::seconds(2))); // bail if handler isnt called in 2 seconds to avoid a stalled test
        auto const& output_ptr = traits.handler().complex_freq_data();
        ASSERT_NE(output_ptr.get(), nullptr);
        verify(*input, *output_ptr, cw_bin);
    }
};

/**
 * @details
 * Carrier wave input tests: CW freq bin is set such that FFT size is an integral multiple of frequency bin.
 * Integral multiple gives precise(bin-centerd) impuse for an injected frequency in the spectrum.
 * Unit tests cover a smaller FFT size 2048(2^11) and longer FFT 8-million(2^23), provides more coverage.
 * CW freq injected should not be more than half of the FFT length to sample above the Nyquist rate.
 * e.g. fft_r2c_cw_2048 tests for FFT size = 2048 & CW freq bin = 256.
 */
ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_cw_2048)
{
    FftR2C_CwTest<TypeParam>::exec(device, 2048, 256);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_cw_8388608)
{
    FftR2C_CwTest<TypeParam>::exec(device, 8388608, 1024);
}

ALGORITHM_TYPED_TEST_P(FftTester, async_fft_r2c_cw_2048)
{
    // Use Case: ensure the c2c interface can be called
    FftR2C_CwTest<TypeParam>::exec_async(device, 2048, 512);
}

/**
 * @details
 * Generate normally distributed noise data with certain mean and standard deviation as per arguments passed.
 * This is a common unit test template for input and output FFT normalization for different mean_n, std_dev_n.
 * Input data is normalized to mean=0, standard deviation=1 before FFT is computed.
 * When normalized Input is passed through FFT and normalized again by sqrt(2/N),
 * then real and imaginary parts of output become Chi-sq distributed with mean=0, standard deviation=1.
 */
template<typename TypeParam>
struct FftR2C_NormTest : public R2C_Base<FftR2C_NormTest<TypeParam>, TypeParam>
{
    static inline
    void run_test(typename TypeParam::DeviceType& device
                 , std::size_t fft_trial_length
                 , float mean_n
                 , float std_dev_n)
    {
        TypeParam traits;
        typedef typename TypeParam::NumericalRep T;
        typedef typename TypeParam::Arch Arch;
        typedef typename TypeParam::ComplexT ComplexT;

        typedef data::TimeSeries<Arch, T> InputType;
        typedef data::FrequencySeries<Arch, ComplexT> OutputType;
        typedef data::TimeSeries<Cpu, T> InputDataCpu;
        typedef data::FrequencySeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> OutputTypeHost;

        // create TimeSeries buffer with sampling period 0.003 s, this can be any value depending on sampling
        auto data_time = data::TimeType(0.003 * data::seconds);
        InputDataCpu data_in(data_time, fft_trial_length);

        /**
        * Input buffer data_in is filled up with normally distributed data using std::mt19937 generator.
        * In this test, data is created with mean_n, std_dev_n arguments passed in different unit tests
        */
        fill_with_random_data(data_in, mean_n, std_dev_n);

        // FFT Input data normalization
        traits.api().template normalize_about_zero<InputDataCpu>(data_in);

        /**
        * Observed tolerances of the data properties are dependent on data type, Boost library and data size.
        * mean_tolerance & std_tolerance variables assigned can be replaced based on design specs
        */
        T mean_tolerance_in = 1e-2;
        T std_tolerance_in = 3e-2;

        // calculate and assert the mean of normalized FFT output
        T abs_mean_diff_with_exp = std::abs(mean<InputDataCpu, T>(data_in) - (T)0);
        ASSERT_TRUE(abs_mean_diff_with_exp < mean_tolerance_in);

        // calculate and assert the standard deviation of normalized FFT output
        T abs_std_dev_diff_with_exp = std::abs(standard_deviation<InputDataCpu, T>(data_in) - (T)1);
        ASSERT_TRUE(abs_std_dev_diff_with_exp < std_tolerance_in);

        // allocate host data on the device buffers using std::allocator_traits
        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));

        // FFT process call
        traits.api().process(device, input, output);

        // check output metadata
        ASSERT_EQ(output.size(), input.size()/2 + 1);
        auto freq_step_exp = (1.0f/(input.sampling_interval().value() * input.size()));
        ASSERT_EQ(output.frequency_step().value(), freq_step_exp);

        // copy output data from device to host buffer
        OutputTypeHost data_out(output);

        // FFT Output data normalization
        // TBD: fft_size to be passed from FFT interface based on input/FFT size
        std::size_t fft_size = pow (2, round(log2((T)(data_in.size()))));
        traits.api().template normalize_sqrt_two_by_length<OutputTypeHost>(data_out, fft_size);

        // separate real & imaginary parts from complex FFT data
        std::vector<T> real_part = {};
        std::vector<T> imag_part = {};
        for(auto const& elm : data_out) {
            real_part.push_back(elm.real());
            imag_part.push_back(elm.imag());
        }

        // Define tolerances for normalized FFT output
        T mean_tolerance_out = 1e-1;
        T std_tolerance_out = 5e-2;

        // calculate and assert the mean of real parts of normalized FFT output
        T abs_mean_diff_with_exp_real = std::abs(mean<std::vector<T>, T>(real_part) - (T)0);
        ASSERT_TRUE(abs_mean_diff_with_exp_real < mean_tolerance_out);

        // calculate and assert the standard deviation of real parts of normalized FFT output
        T abs_std_dev_diff_with_exp_real = std::abs(standard_deviation<std::vector<T>, T>(real_part) - (T)1);
        ASSERT_TRUE(abs_std_dev_diff_with_exp_real < std_tolerance_out);

        // calculate and assert the mean of imaginary parts of normalized FFT output
        T abs_mean_diff_with_exp_imag = std::abs(mean<std::vector<T>, T>(imag_part) - (T)0);
        ASSERT_TRUE(abs_mean_diff_with_exp_imag < mean_tolerance_out);

        // calculate and assert the standard deviation of imaginary parts of normalized FFT output
        T abs_std_dev_diff_with_exp_imag = std::abs(standard_deviation<std::vector<T>, T>(imag_part) - (T)1);
        ASSERT_TRUE(abs_std_dev_diff_with_exp_imag < std_tolerance_out);
    }

};

/**
 * @details
 * FFT normalization tests: Gaussian noise data is pre-normalized, Fourier transformed and post-normalized.
 * Noise inputs of different data lengths, mean and standard deviation are unit testet below
 * Mean=64*4096 and standard_deviation=20*64 tests a larger possible combination of DDTR output/CXFT input
 * FFT size = 2048 for a smaller range and FFT size = 8388608 for a larger range.
 * e.g. fft_r2c_norm_2048_0_1 tests fft_size=2048, mean=0 and standard_deviation=1, i.e.already normalized input
 */
ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_norm_2048_0_1)
{
    FftR2C_NormTest<TypeParam>::exec(device, 2048, 0, 1);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_norm_2048_5_10)
{
    FftR2C_NormTest<TypeParam>::exec(device, 2048, 5, 10);
}

// a combination of larger positive mean & standard_deviation within the limit of observed tolerance
ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_norm_2048_max)
{
    FftR2C_NormTest<TypeParam>::exec(device, 2048, 64*4096, 20*64);
}

// a combination of larger negative mean & standard_deviation within the limit of observed tolerance
ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_norm_2048_min)
{
    FftR2C_NormTest<TypeParam>::exec(device, 2048, -64*4096, 20*64);
}

// already normalized data as an input
ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_norm_8388608_0_1)
{
    FftR2C_NormTest<TypeParam>::exec(device, 8388608, 0, 1);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_norm_8388608_5_10)
{
    FftR2C_NormTest<TypeParam>::exec(device, 8388608, 5, 10);
}

// a combination of larger positive mean & standard_deviation within the limit of observed tolerance
ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_norm_8388608_max)
{
    FftR2C_NormTest<TypeParam>::exec(device, 8388608, 64*4096, 20*64);
}

// A combination of larger negative mean & standard_deviation within the limit of observed tolerance
ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_norm_8388608_min)
{
    FftR2C_NormTest<TypeParam>::exec(device, 8388608, -64*4096, 20*64);
}


/**
 * @details
 * Unit testing of input data normalization for before Fourier Transform.
 * TimeSeries noise data is normalized to mean=0, standard deviation=1.
 */
TYPED_TEST_P(FftTester, time_normalize_to_zero_mean)
{
    TypeParam traits;
    typedef typename TypeParam::NumericalRep T;
    typedef data::TimeSeries<Cpu, data::ConvertComplexNumber_t<Cpu, T>> InputDataCpuTime;

    std::size_t size = 1024;
    auto data_time = data::TimeType(0.003 * data::seconds);
    InputDataCpuTime data_in(data_time, size);

    /**
    * Input buffer data_in is filled up with normally distributed data using std::mt19937 generator.
    * In this test, data is created with examplary mean=10, standard_deviation=2
    */
    fill_with_random_data(data_in, 10, 2);

    // data normalization about zero Mean
    traits.api().template normalize_about_zero<InputDataCpuTime>(data_in);

    /**
    * Observed tolerances of the data properties are dependent on data type, Boost library and data size.
    * mean_tolerance & std_tolerance variables assigned below can be replaced based on design specs
    */
    T mean_tolerance = 5e-2;
    T std_tolerance = 1e-1;

    // get the mean of normalized data and assert with expected value
    T abs_mean_diff_with_exp = std::abs(mean<InputDataCpuTime, T>(data_in) - (T)0);
    ASSERT_TRUE(abs_mean_diff_with_exp < mean_tolerance);

    // get the standard deviation of normalized data and assert with expected value
    T abs_std_dev_diff_with_exp = std::abs(standard_deviation<InputDataCpuTime, T>(data_in) - (T)1);
    ASSERT_TRUE((abs_std_dev_diff_with_exp - (T)1) < std_tolerance);

}

/**
 * @details
 * Unit testing of input data normalization for before Fourier Transform.
 * FrequencySeries noise data is normalized to mean=0, standard deviation=1.
 */
TYPED_TEST_P(FftTester, freq_normalize_to_zero_mean)
{
    TypeParam traits;
    typedef typename TypeParam::NumericalRep T;

    // create FrequencySeries host buffer type for given data type
    typedef data::FrequencySeries<Cpu, data::ConvertComplexNumber_t<Cpu, T>> InputDataCpuFreq;


    /**
    * Create data_in buffer to test the data is not normalized. Size can be any value, hence 1024 assigned.
    * data_in buffer is created with sampling freq 5 Hz, this can be any value depending on sampling.
    */
    std::size_t size = 1024;
    auto data_freq = data::FourierFrequencyType(5 * data::hz);
    InputDataCpuFreq data_in(data_freq, size);

    /**
    * Input buffer data_in is filled up with normally distributed data using std::mt19937 generator.
    * In this test, data is created with examplary mean=10, standard_deviation=2
    */
    fill_with_random_data(data_in, 10, 2);

    // data normalization about zero Mean
    traits.api().template normalize_about_zero<InputDataCpuFreq>(data_in);

    /**
    * Observed tolerances of the data properties are dependent on data type, Boost library and data size.
    * mean_tolerance & std_tolerance variables assigned below can be replaced based on design specs
    */
    T mean_tolerance = 5e-2;
    T std_tolerance = 1e-1;

    // get the mean of normalized data and assert with expected value
    T abs_mean_diff_with_exp = std::abs(mean<InputDataCpuFreq, T>(data_in) - (T)0);
    ASSERT_TRUE(abs_mean_diff_with_exp < mean_tolerance);

    // get the standard deviation of normalized data and assert with the expected values
    T abs_std_dev_diff_with_exp = std::abs(standard_deviation<InputDataCpuFreq, T>(data_in) - (T)1);
    ASSERT_TRUE(abs_std_dev_diff_with_exp < std_tolerance);

}

/**
 * @details
 * Unit testing of input normalization with unsigned data, displays only warning message.
 */
TYPED_TEST_P(FftTester, unsigned_normalize_to_zero_mean)
{
    TypeParam traits;
    typedef data::TimeSeries<Cpu, uint8_t> InputDataCpuTime;

    /**
    * Create TimeSeries buffers to test the data is not normalized. Size can be any value, hence, 1024 assigned.
    * TimeSeries buffers are created with sampling period 0.003 s, this can be any value depending on sampling.
    */
    std::size_t size = 1024;
    auto data_time = data::TimeType(0.003 * data::seconds);
    InputDataCpuTime data_in(data_time, size);
    InputDataCpuTime data_in_copy(data_time, size);

    // fill data_in buffer with random data
    for(auto& val : data_in) {
        val = rand();
    }

    // create a copy into another buffer to verify normalization
    std::copy(data_in.begin(), data_in.end(), data_in_copy.begin());

    // data normalization about zero Mean
    traits.api().template normalize_about_zero<InputDataCpuTime>(data_in);

    // verify data has not been changed in case of unsigned data type
    ASSERT_EQ(data_in.size(), data_in_copy.size());

    auto copy_it = data_in_copy.begin();
    for(auto const& val : data_in) {
        // check the data is intact
        ASSERT_EQ(val, *copy_it);
        ++copy_it;
    }
}

/**
 * @details
 * Unit testing of output data normalization for after Fourier Transform.
 * TimeSeries constant data is normalized to by square root of ( 2 / size)
 */
TYPED_TEST_P(FftTester, time_normalize_sqrt_2_by_size)
{
    TypeParam traits;
    typedef typename TypeParam::NumericalRep T;
    typedef typename TypeParam::ComplexT ComplexT;

    // create TimeSeries host buffer type for complex data
    typedef data::TimeSeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> InputDataCpuTime;


    /**
    * Create data_in buffer to test the data is normalized. Size can be any value, hence, 1024 is assigned.
    * data_in buffer is created with sampling period 0.003 s, this can be any value depending on sampling.
    */
    std::size_t size = 1024;
    T norm_factor = sqrt(2.0 / size);
    auto data_time = data::TimeType(0.003 * data::seconds);
    InputDataCpuTime data_in(data_time, size);

    // Data is complex, real & imag parts are assigned with initial value of x=1, it can be any number
    T x = 1;
    for(auto& elm : data_in) {
        elm.real(x);
        elm.imag(x);
    }

    // data normalization by sqrt(2/size)
    traits.api().template normalize_sqrt_two_by_length<InputDataCpuTime>(data_in, size);

    // norm_tolerance can be used to define precision of normalized data, value depneds on data type
    T norm_tolerance = 1e-10;
    auto abs_diff_with_exp_real = 0.0;
    auto abs_diff_with_exp_imag = 0.0;
    for(auto& elm : data_in) {
        abs_diff_with_exp_real = std::abs(elm.real() - (norm_factor * x));
        abs_diff_with_exp_imag = std::abs(elm.imag() - (norm_factor * x));
        ASSERT_TRUE(abs_diff_with_exp_real < norm_tolerance);
        ASSERT_TRUE(abs_diff_with_exp_imag < norm_tolerance);
    }
}

/**
 * @details
 * Unit testing of output data normalization for after Fourier Transform.
 * FrequencySeries constant data is normalized to by square root of ( 2 / size)
 */
TYPED_TEST_P(FftTester, freq_normalize_sqrt_2_by_size)
{
    TypeParam traits;
    typedef typename TypeParam::NumericalRep T;
    typedef typename TypeParam::ComplexT ComplexT;

    // create FrequencySeries host buffer type for complex data
    typedef data::FrequencySeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> InputDataCpuFreq;


    /**
    * Create data_in buffer to test the data is normalized. Size can be any value, hence, 1024 is assigned.
    * data_in buffer is created with sampling freq 5 Hz, this can be any value depending on sampling.
    * Input is complex data, real & imag parts are assigned with initial value of x=1, it can be any number.
    */
    std::size_t size = 1024;
    T norm_factor = sqrt(2.0 / size);
    auto data_freq = data::FourierFrequencyType(5 * data::hz);
    InputDataCpuFreq data_in(data_freq, size);

    // Data is complex, real & imag parts are assigned with initial value of x=1, it can be any number
    T x = 1;
    for(auto& elm : data_in) {
        elm.real(x);
        elm.imag(x);
    }

    // data normalization by sqrt(2/size)
    traits.api().template normalize_sqrt_two_by_length<InputDataCpuFreq>(data_in, size);

    // norm_tolerance can be used to define precision of normalized data, value depneds on data type
    T norm_tolerance = 1e-10;
    auto abs_diff_with_exp_real = 0.0;
    auto abs_diff_with_exp_imag = 0.0;
    for(auto& elm : data_in) {
        abs_diff_with_exp_real = std::abs(elm.real() - (norm_factor * x));
        abs_diff_with_exp_imag = std::abs(elm.imag() - (norm_factor * x));
        ASSERT_TRUE(abs_diff_with_exp_real < norm_tolerance);
        ASSERT_TRUE(abs_diff_with_exp_imag < norm_tolerance);
    }
}

/**
 * @details
 * Insert a sine wave where FFT length is integral multiple of bin number of the carrier frequency
 * For a scaled input, we expect the impulse of magnitude unity for given the impulse inserted by cw_bin
 * C2C is defined for even power of 2 as it gives square matrix to calculate row/column iterations
 * TBD: truncation/zero-padding of input data is not yet included, data length is expected to be exact power of 2
 */
template<typename TypeParam>
struct FftC2C_CwTest : public C2C_Base<FftC2C_CwTest<TypeParam>, TypeParam>
{
    /// TimeSeries<Complex> -> FrequencySeries<Complex>
    typedef typename TypeParam::Arch Arch;
    typedef typename TypeParam::NumericalRep T;
    typedef typename TypeParam::ComplexT ComplexT;
    typedef data::TimeSeries<Arch, ComplexT> InputType;
    typedef data::FrequencySeries<Arch, ComplexT> OutputType;
    typedef data::TimeSeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> InputDataCpu;
    typedef data::FrequencySeries<Cpu, data::ConvertComplexNumber_t<Cpu, ComplexT>> OutputTypeHost;
    typedef typename TypeParam::DeviceType Device;

    static
    std::size_t calc_fft_size(std::size_t fft_trial_length)
    {
        return pow(2, round(log2((T)(fft_trial_length)))); // FIXME should use the plan
    }

    static
    std::unique_ptr<InputDataCpu> generate_input_data(std::size_t fft_trial_length, std::size_t cw_bin)
    {
        // create TimeSeries buffer with sampling period 0.003 s, this can be any value depending on sampling
        std::unique_ptr<InputDataCpu> data_in(new InputDataCpu(data::TimeType(0.003 * data::seconds), fft_trial_length));

        size_t cw_samples = calc_fft_size(fft_trial_length)/cw_bin;
        size_t count_input = 0;

        // fill up input buffer with sine wave data, scaled by 2 to get unity impluse after FFT
        for(auto& elm : *data_in) {
            elm = 2 * sin((2 * (boost::math::constants::pi<T>()) / cw_samples) * count_input);
            ++count_input;
        }

        return data_in;
    }

    template<typename InputT, typename OutputT>
    static
    void verify(InputT const& input, OutputT const& output, std::size_t cw_bin) {
        ASSERT_EQ(output.size(), input.size());
        ASSERT_EQ(output.frequency_step().value(), (1.0f/(input.sampling_interval().value() * input.size())));

        size_t fft_size = calc_fft_size(input.size());

        // verfiy forward transform has calculated FFT with a certain accuracy e.g fwd_accuracy = 1e-4
        T fwd_accuracy = 1e-4;

        // copy output data from device to host buffer
        OutputTypeHost data_out(output);

        size_t count_out = 0;
        for(auto const& elm : data_out)
        {
            SCOPED_TRACE(std::to_string(count_out));
            // calculate and normalize output power by FFT length
            double cw_power = (sqrt(elm.real()*elm.real()+elm.imag()*elm.imag())/fft_size);

            // carrier wave power is checked for the injected frequency bin, scaled input gives unit output power
            if (count_out == cw_bin || count_out == (fft_size - cw_bin) ) {
                ASSERT_FLOAT_EQ(round(cw_power), (T)1);
            }
            else {
                ASSERT_TRUE(std::abs((T)(cw_power)-(T)0) < fwd_accuracy);
            }

            ++count_out;
        }

    }

    template<typename OutputT>
    static
    void verify_inverse(InputDataCpu const& input, OutputT const& output) {
        ASSERT_EQ(output.size(), input.size());
        ASSERT_EQ(input.sampling_interval().value(), output.sampling_interval().value());

        size_t fft_size = calc_fft_size(input.size());

        // copy inverse transform data from device to host buffer and verify it's size
        InputDataCpu inverse_host(output);
        ASSERT_EQ(inverse_host.size(), output.size());

        // verfiy inverse transform has returned input data with a certain accuracy e.g inv_accuracy = 1e-5
        T inv_accuracy = 1e-5;
        auto inv_it = inverse_host.begin();
        size_t count_inv = 0;

        for(auto const& elm : input)
        {
            SCOPED_TRACE(std::to_string(count_inv));
            auto inv_itr = *inv_it;
            ASSERT_TRUE(std::abs(elm.real() - inv_itr.real() / fft_size) < inv_accuracy)
                       << "expected:" << elm.real()  << " got:" << inv_itr.real() / fft_size
                       << " accuracy:" << inv_accuracy;

            ++inv_it;
            ++count_inv;
        }
    }

    static inline
    void run_test(typename TypeParam::DeviceType& device, std::size_t fft_trial_length, std::size_t cw_bin)
    {
        TypeParam traits;
        auto data_in_ptr = generate_input_data(fft_trial_length, cw_bin);
        InputDataCpu const& data_in = *data_in_ptr;

        // allocate host data on the device buffers using std::allocator_traits
        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));

        // FFT process call
        traits.api().process(device, input, output);
        verify(input, output, cw_bin);

        // perform inverse transform (i.e a c2c transform) if it defined for the algorithm
        InputType inverse(traits.template allocator<InputType>(device));
        if(! process(traits, device, output, inverse))
        {
            std::cout << "Inverse transform not availaable - skipping test\n";
            return;
        }
        verify_inverse(data_in, inverse);
    }

    // asynctest
    static inline
    void exec_async(Device& device, std::size_t fft_trial_length, std::size_t cw_bin)
    {
        TypeParam traits;
        auto data_in_ptr = generate_input_data(fft_trial_length, cw_bin);
        InputDataCpu const& data_in = *data_in_ptr;

        std::shared_ptr<InputType> input = std::make_shared<InputType>(data_in, traits.template allocator<InputType>(device));

        auto job = traits.api().fft_to_complex(input);
        job->wait();
        ASSERT_TRUE(traits.handler().wait(std::chrono::seconds(60))); // bail if the handler has not received a callback in 60 seconds
        auto const& output_ptr = traits.handler().complex_freq_data();
        ASSERT_NE(output_ptr.get(), nullptr);
        verify(*input, *output_ptr, cw_bin);

        // Inverse transform
        job = traits.api().fft_to_complex(output_ptr);
        job->wait();
        auto const& inv_output_ptr = traits.handler().complex_time_data();
        ASSERT_NE(inv_output_ptr.get(), nullptr);
        verify_inverse(data_in, *inv_output_ptr);
    }
};


/**
 * @details
 * Carrier wave input tests: CW freq bin is set such that FFT size is an integral multiple of frequency bin.
 * Integral multiple gives precise(bin-centerd) impuse for an injected frequency in the spectrum.
 * Unit tests cover a smaller FFT size 1024(2^10) and longer FFT 8-million(2^22), provides more coverage.
 * CW freq injected should not be more than half of the FFT length to sample above the Nyquist rate.
 * e.g. fft_c2c_cw_1024 tests for FFT size = 1024 & CW freq bin = 256.
 */
ALGORITHM_TYPED_TEST_P(FftTester, fft_c2c_cw_1024)
{
    FftC2C_CwTest<TypeParam>::exec(device, 1024, 256);
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_c2c_cw_4194304)
{
    FftC2C_CwTest<TypeParam>::exec(device, 4194304, 1024);
}

ALGORITHM_TYPED_TEST_P(FftTester, async_fft_c2c_cw_1024)
{
    // Use Case: ensure the c2c interface can be called
    FftC2C_CwTest<TypeParam>::exec_async(device, 1024, 256);
}

/// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
/// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(FftTester
                          , fft_r2c_zero_128
                          , fft_r2c_zero_2048
                          , fft_r2c_zero_8388608
                          , fft_r2c_c2r_delta_128
                          , fft_r2c_c2r_delta_2048
                          , fft_r2c_c2r_delta_8388608
                          , fft_r2c_shift_128
                          , fft_r2c_shift_2048
                          , fft_r2c_shift_8388608
                          , fft_r2c_cw_2048
                          , fft_r2c_cw_8388608
                          , fft_c2c_cw_1024
                          , async_fft_r2c_cw_2048
                          , fft_c2c_cw_4194304
                          , fft_r2c_norm_2048_0_1
                          , fft_r2c_norm_2048_5_10
                          , fft_r2c_norm_2048_max
                          , fft_r2c_norm_2048_min
                          , fft_r2c_norm_8388608_0_1
                          , fft_r2c_norm_8388608_5_10
                          , fft_r2c_norm_8388608_max
                          , fft_r2c_norm_8388608_min
                          , time_normalize_to_zero_mean
                          , freq_normalize_to_zero_mean
                          , unsigned_normalize_to_zero_mean
                          , time_normalize_sqrt_2_by_size
                          , freq_normalize_sqrt_2_by_size
                          , async_fft_c2c_cw_1024);
} // namespace test
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
