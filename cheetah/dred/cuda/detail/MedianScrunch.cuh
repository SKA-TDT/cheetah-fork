#ifndef SKA_CHEETAH_DRED_CUDA_DETAIL_MEDIANSCRUNCH_H
#define SKA_CHEETAH_DRED_CUDA_DETAIL_MEDIANSCRUNCH_H

#include "cheetah/cuda_utils/cuda_thrust.h"

namespace ska {
namespace cheetah {
namespace dred {
namespace cuda {
namespace detail {

template <typename T> inline __host__ __device__ T median3(T a, T b, T c);
template <typename T> inline __host__ __device__ T median4(T a, T b, T c, T d);
template <typename T> inline __host__ __device__ T median5(T a, T b, T c, T d, T e);

template <typename T>
struct Median5Functor: public thrust::unary_function<T,T>
{
    const T* _in;
    Median5Functor(const T* in);
    inline __host__ __device__ T operator()(unsigned int i) const;
};

template <typename T>
struct LinearStretchFunctor: public thrust::unary_function<T,T>
{
    const T* _in;
    unsigned _in_size;
    float _step;
    float _correction;
    LinearStretchFunctor(const T* in, unsigned in_size, float step);
    inline __host__ __device__ T operator()(unsigned out_idx) const;
};


} // namespace detail
} // namespace cuda
} // namespace dred
} // namespace cheetah
} // namespace ska

#include "cheetah/dred/cuda/detail/MedianScrunch.cu"

#endif // SKA_CHEETAH_DETAIL_MEDIANSCRUNCH_H
