/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/test_utils/TimeSeriesTest.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/Units.h"

namespace ska {
namespace cheetah {
namespace data {
namespace test {

template <typename TimeSeriesTestTraitsType>
TimeSeriesTest<TimeSeriesTestTraitsType>::TimeSeriesTest()
    : ::testing::Test()
{
}

template <typename TimeSeriesTestTraitsType>
TimeSeriesTest<TimeSeriesTestTraitsType>::~TimeSeriesTest()
{
}

template <typename TimeSeriesTestTraitsType>
void TimeSeriesTest<TimeSeriesTestTraitsType>::SetUp()
{
}

template <typename TimeSeriesTestTraitsType>
void TimeSeriesTest<TimeSeriesTestTraitsType>::TearDown()
{
}


template <typename TypeParam>
void SampleCountTest<TypeParam>::test(std::size_t nsamples)
{
    std::size_t number_of_samples = nsamples;
    TimeType dt = 5.4 * seconds;
    TimeSeries<typename TypeParam::Architecture,typename TypeParam::ValueType> timeseries(dt);
    timeseries.resize(number_of_samples);
    timeseries[0] = 3;
    ASSERT_EQ(timeseries[0],3);
    ASSERT_EQ(number_of_samples,timeseries.size());
    ASSERT_EQ(dt,timeseries.sampling_interval());
}

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
