/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace ska {
namespace cheetah {
namespace data {


template <typename Arch, typename T>
Candidate<Arch,T>::Candidate()
{
}


template <typename Arch, typename T>
Candidate<Arch,T>::Candidate(Candidate<Arch, T>::MsecTimeType period, SecPerSecType pdot, Candidate<Arch,T>::Dm dm, std::size_t ident)
    : _pulse_period(period)
    , _pulse_pdot(pdot)
    , _dm(dm)
    , _ident(ident)
{
}


template <typename Arch, typename T>
Candidate<Arch,T>::Candidate(Candidate<Arch, T>::MsecTimeType period, SecPerSecType pdot, Candidate<Arch, T>::Dm dm, Candidate<Arch, T>::MsecTimeType width, T sigma, std::size_t ident)
    : _pulse_period(period)
    , _pulse_pdot(pdot)
    , _dm(dm)
    , _pulse_width(width)
    , _sigma(sigma)
    , _ident(ident)
{
}

template <typename Arch, typename T>
Candidate<Arch,T>::~Candidate(){}


template <typename Arch, typename T>
typename Candidate<Arch,T>::MsecTimeType const& Candidate<Arch,T>::period() const
{
    return this->_pulse_period;
}

template <typename Arch, typename T>
void  Candidate<Arch,T>::period(Candidate<Arch,T>::MsecTimeType c_pulse_period)
{
    this->_pulse_period = c_pulse_period;
}

template <typename Arch, typename T>
typename Candidate<Arch,T>::SecPerSecType const& Candidate<Arch,T>::pdot() const
{
    return this->_pulse_pdot;
}

template <typename Arch, typename T>
void  Candidate<Arch,T>::pdot(SecPerSecType c_pulse_pdot)
{
    this->_pulse_pdot = c_pulse_pdot;
}

template <typename Arch, typename T>
typename Candidate<Arch,T>::Dm const & Candidate<Arch,T>::dm() const
{
    return this->_dm;
}

template <typename Arch, typename T>
void Candidate<Arch,T>::dm(Dm c_dm)
{
    this->_dm = c_dm;
}


template <typename Arch, typename T>
typename Candidate<Arch,T>::MsecTimeType const& Candidate<Arch,T>::width() const
{
    return this->_pulse_width;
}

template <typename Arch, typename T>
void  Candidate<Arch,T>::width(Candidate<Arch,T>::MsecTimeType c_pulse_width)
{
    this->_pulse_width = c_pulse_width;
}


template <typename Arch, typename T>
T const& Candidate<Arch,T>::sigma() const
{
    return this->_sigma;
}

template <typename Arch, typename T>
void  Candidate<Arch,T>::sigma(T c_sigma)
{
    this->_sigma = c_sigma;
}

template <typename Arch, typename T>
std::size_t const& Candidate<Arch,T>::ident() const
{
    return (this->_ident);
}

template <typename Arch, typename T>
void Candidate<Arch,T>::ident(std::size_t c_ident)
{
    this->_ident = c_ident;
}

} // namespace data
} // namespace cheetah
} // namespace ska



