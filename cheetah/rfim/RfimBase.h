#ifndef SKA_CHEETAH_RFIM_RFIMBASE_H
#define SKA_CHEETAH_RFIM_RFIMBASE_H

#include "cheetah/rfim/PolicyInfo.h"
#include "cheetah/rfim/policy/Policy.h"

namespace ska {
namespace cheetah {
namespace rfim {

/**
 * @brief
 *   Wrap a Rfim flaggeing style detector and adjust the data according to the flags and a specific policy
 *
 * @details
 *
 */

template<class RfimDetector, class RfimPolicy>
class RfimBase
{
    protected:
        typedef PolicyInfo<RfimPolicy> InfoType;
        typedef typename InfoType::DataArgumentType DataType;
        typedef typename std::shared_ptr<typename std::remove_reference<DataType>::type> DataTypePtr;

    public:
        typedef typename InfoType::AdapterType DataAdapter;
        typedef typename InfoType::ReturnType ReturnType;

    public:
        RfimBase();
        ~RfimBase();

        template<typename ResourceType>
        ReturnType operator()(ResourceType&&, DataTypePtr data);

        ReturnType operator()(panda::PoolResource<Cpu>&, DataTypePtr data );

    private:
        policy::Policy<RfimPolicy> _policy;
};


} // namespace rfim
} // namespace cheetah
} // namespace ska
#include "cheetah/rfim/detail/RfimBase.cpp"

#endif // SKA_CHEETAH_RFIM_RFIMBASE_H
