subpackage(simple_sift)

set(MODULE_SIFT_LIB_SRC_CPU
    src/Config.cpp
    src/Sift.cpp
    ${LIB_SRC_CPU}
    PARENT_SCOPE
)

test_utils()
add_subdirectory(test)
