/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sift/SiftModule.h"

namespace ska {
namespace cheetah {
namespace sift {

template<class SiftTraits, typename... SiftAlgos>
SiftModule<SiftTraits, SiftAlgos...>::SiftModule(Config const& config, Handler& handler)
    : BaseT(config.pool(), SiftAlgoFactory<SiftTraits>(config), handler)
{
}

template<class SiftTraits, typename... SiftAlgos>
std::shared_ptr<panda::ResourceJob> SiftModule<SiftTraits, SiftAlgos...>::operator()(std::shared_ptr<data::Ccl> const& input) const
{
    return BaseT::operator()(input);
}

template<class SiftTraits, typename... SiftAlgos>
template< typename OtherDataType
        , typename std::enable_if<std::is_convertible<OtherDataType, data::Ccl>::value, bool>::type
        >
std::shared_ptr<panda::ResourceJob> SiftModule<SiftTraits, SiftAlgos...>::operator()(std::shared_ptr<OtherDataType> const& input)
{
    return BaseT::operator()(std::static_pointer_cast<data::Ccl>(input));
}

} // namespace sift
} // namespace cheetah
} // namespace ska
