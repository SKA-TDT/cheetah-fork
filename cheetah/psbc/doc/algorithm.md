@page psbc_algorithm_guide PSBC algorithm guide
# Notes for Developers
## PSBC algorithm
This algorithm will add a block of DmTrials data to a buffer. First, if there are data already buffered, it will check if the block of data being added is compatible (of the same data type) to the data already in the buffer. If so, it will add the block to the buffer. Once the buffer is full, i.e. has met or exceeded the set duration, it will check if the data blocks in the buffer are contiguous in time. If so, the buffer will flush itself by calling a handler, then clear the data and set its duration to zero.
