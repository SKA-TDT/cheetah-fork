include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_psbc_src
    src/PsbcTest.cpp
    src/gtest_psbc.cpp
)

add_executable(gtest_psbc ${gtest_psbc_src})
target_link_libraries(gtest_psbc ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_psbc gtest_psbc --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
