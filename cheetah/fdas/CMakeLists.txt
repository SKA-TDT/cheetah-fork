subpackage(opencl)

set(MODULE_FDAS_LIB_SRC_CPU
    src/Config.cpp
    ${LIB_SRC_CPU}
    PARENT_SCOPE
)

test_utils()
add_subdirectory(test)
