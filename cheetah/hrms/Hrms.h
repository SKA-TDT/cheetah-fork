/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_HRMS_HARMONICSUMMING_H
#define SKA_CHEETAH_HRMS_HARMONICSUMMING_H

#include "cheetah/hrms/Config.h"
#include "cheetah/hrms/cuda/Hrms.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/data/Units.h"
#include "cheetah/utils/Architectures.h"

#include "panda/AlgorithmTuple.h"

#include <vector>


namespace ska {
namespace cheetah {
namespace hrms {

/**
 * @brief      A class for performing harmonic summing
 */
class Hrms
{
    private:
        typedef panda::AlgorithmTuple<cuda::Hrms> Implementations;

    public:
        /**
         * @brief      Construct a new Hrms object
         *
         * @param      config  A configuration object for the Hrms instance
         */
        Hrms(Config const& config);
        Hrms(Hrms const&) = delete;
        Hrms(Hrms&&) = default;
        ~Hrms();

        /**
         * @brief      Perform harmonic summing on a frequency series
         *
         * @details    This is a forwarding call that will pass its
         *             arguments onto the first implementation that
         *             provides a matching method. The implementation
         *             must ensure that the correct size and metadata is
         *             set on each of the output FrequencySeries before
         *             performing the sum.
         *
         * @param[in]      resource   A panda::PoolResource instance specifying the resource to process on
         * @param[in]      input      The FrequencySeries instance on which harmonic summing will be performed
         * @param[out]     output     A FrequencySeries instance for each harmonic sum that is performed
         * @param          args       Additional arguments to be passed to the implementation
         *
         * @tparam     Arch       The cheetah architecture type
         * @tparam     T          The value type of the input and output FrequencySeries
         * @tparam     Alloc      The allocator type of the input and output FrequencySeries
         * @tparam     Args       The types of additional arguments to be forwarded
         */
        template <typename Arch, typename T, typename Alloc, typename... Args>
        void process(panda::PoolResource<Arch>& resource,
            data::PowerSeries<Arch,T,Alloc> const& input,
            std::vector<data::PowerSeries<Arch,T,Alloc>>& output,
            Args&&... args);

    private:
        Config const& _config;
        Implementations _implementations;

};

} // namespace hrms
} // namespace cheetah
} // namespace ska

#include "cheetah/hrms/detail/Hrms.cpp"

#endif // SKA_CHEETAH_HRMS_HARMONICSUMMING_H
