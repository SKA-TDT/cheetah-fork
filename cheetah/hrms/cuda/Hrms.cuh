#ifndef SKA_CHEETAH_HRMS_CUDA_HRMS_CUH
#define SKA_CHEETAH_HRMS_CUDA_HRMS_CUH

#include "cheetah/hrms/cuda/Config.h"
#include "cheetah/hrms/Config.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/data/Units.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/utils/Architectures.h"
#include "panda/arch/nvidia/DeviceCapability.h"

#include <vector>

namespace ska {
namespace cheetah {
namespace hrms {
namespace cuda {

/**
 * @brief
 *   CUDA/Thrust implementation of the Hrms algorithm
 */

class Hrms: public utils::AlgorithmBase<Config, hrms::Config>
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability;
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        /**
         * @brief      Construct an instance of Hrms
         *
         * @param      config  The Hrms configuration
         */
        Hrms(Config const& config, hrms::Config const& algo_config);
        Hrms(Hrms const&) = delete;
        Hrms(Hrms&&) = default;
        ~Hrms();

        /**
         * @brief      Perform harmonic summing of a PowerSeries object
         *
         * @param      gpu     The device on which to process
         * @param      input   The input PowerSeries object
         * @param      output  A PowerSeries object for each sum output
         *
         * @tparam     T       The value type of the input and outputs
         * @tparam     Alloc   The allocator types of the inputs and outputs
         */
        template <typename T, typename Alloc>
        void process(ResourceType& gpu,
            data::PowerSeries<cheetah::Cuda,T,Alloc> const& input,
            std::vector<data::PowerSeries<cheetah::Cuda,T,Alloc>>& output);
};

} //cuda
} //hrms
} //cheetah
} //ska

#include "cheetah/hrms/cuda/detail/Hrms.cu"

#endif //SKA_CHEETAH_HRMS_CUDA_HRMS_CUH







