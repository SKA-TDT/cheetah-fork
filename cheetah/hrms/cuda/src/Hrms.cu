#include "cheetah/hrms/cuda/Hrms.cuh"

namespace ska {
namespace cheetah {
namespace hrms {
namespace cuda {

Hrms::Hrms(Config const& config, hrms::Config const& algo_config)
    : utils::AlgorithmBase<Config, hrms::Config>(config,algo_config)
{
}

Hrms::~Hrms()
{
}

} //cuda
} //hrms
} //cheetah
} //ska
