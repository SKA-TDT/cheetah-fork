include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_pwft_src_cpu
    src/gtest_pwft.cpp
#    src/Pwft-TDT-Reference.cpp
#    src/Pwft-TDT-Reference-Interface.cpp
)

set(gtest_pwft_src_cuda
#    src/PwftTest.cu
)

if(ENABLE_CUDA)
    cuda_add_executable(gtest_pwft ${gtest_pwft_src_cuda} ${gtest_pwft_src_cpu})
else()
    add_executable(gtest_pwft ${gtest_pwft_src_cpu})
endif()

target_link_libraries(gtest_pwft ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_pwft gtest_pwft)
