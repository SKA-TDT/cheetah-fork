#include "cheetah/pwft/cuda/Pwft.cuh"


namespace ska {
namespace cheetah {
namespace pwft {
namespace cuda {


Pwft::Pwft(Config const& config, pwft::Config const& algo_config)
    : utils::AlgorithmBase<Config, pwft::Config>(config,algo_config)
{
}

Pwft::~Pwft()
{
}

} // namespace cuda
} // namespace pwft
} // namespace cheetah
} // namespace ska
