#ifndef SKA_CHEETAH_PWFT_CUDA_PWFT_CUH
#define SKA_CHEETAH_PWFT_CUDA_PWFT_CUH

#include "cheetah/pwft/cuda/Config.h"
#include "cheetah/pwft/Config.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/cuda_utils/cuda_thrust.h"
#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace cheetah {
namespace pwft {
namespace cuda {


/**
 * @brief      Cuda implementation of the Power Spectrum Fourier Transform module
 */

class Pwft: utils::AlgorithmBase<Config, pwft::Config>
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability;
        typedef panda::PoolResource<Architecture> ResourceType;

    public:

        /**
         * @brief      Construct a new instance
         *
         * @param      config  A cuda::Pwft configuration object
         */
        Pwft(Config const& config, pwft::Config const& algo_config);
        Pwft(Pwft const&) = delete;
        Pwft(Pwft&&) = default;
        ~Pwft();

        /**
         * @brief      Form power spectrum using absolute squared.
         *
         * @details     Each power measurement is found by
         *             output[i] = |input[i]|^2
         *
         * @param      gpu        The gpu resource to process on
         * @param[in]  input      A FrequencySeries object of complex data type
         * @param[out] output     A FrequencySeries object of real data type
         *
         * @tparam     T             The value type of the input and output FrequencySeries
         * @tparam     InputAlloc    The allocator type of the input FrequencySeries
         * @tparam     OutputAlloc   The allocator type of the output FrequencySeries
         */

        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process_direct(ResourceType& gpu,
            data::FrequencySeries<Architecture,typename data::ComplexTypeTraits<cheetah::Cuda,T>::type, InputAlloc> const& input,
            data::PowerSeries<Architecture,T,OutputAlloc>& output);


        /**
         * @brief      Form power spectrum using absolute squared with nearest neighbour comparison.
         *
         * @details     Each power measurement is found by comparing neighbouring bins such that
         *             output[i] = max(|input[i]|^2, |input[i]-input[i-1]|^2)
         *
         * @param      gpu        The gpu resource to process on
         * @param[in]  input      A FrequencySeries object of complex data type
         * @param[out] output     A FrequencySeries object of real data type
         *
         * @tparam     T             The value type of the input and output FrequencySeries
         * @tparam     InputAlloc    The allocator type of the input FrequencySeries
         * @tparam     OutputAlloc   The allocator type of the output FrequencySeries
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process_nn(ResourceType& gpu,
            data::FrequencySeries<Architecture,typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,InputAlloc> const& input,
            data::PowerSeries<Architecture,T,OutputAlloc>& output);

    private:
};

} //cuda
} //pwft
} //cheetah
} //ska

#include "cheetah/pwft/cuda/detail/Pwft.cu"

#endif //SKA_CHEETAH_PWFT_CUDA_PWFT_H
