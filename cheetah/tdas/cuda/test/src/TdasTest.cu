#include "cheetah/tdas/cuda/Tdas.cuh"
#include "cheetah/tdas/test_utils/TdasTester.h"

namespace ska {
namespace cheetah {
namespace tdas {
namespace cuda {
namespace test {

template <typename T>
struct CudaTraits
    : public tdas::test::TdasTesterTraits<typename tdas::cuda::Tdas<T>::Architecture, typename tdas::cuda::Tdas<T>::ArchitectureCapability, T>
{
    typedef tdas::cuda::Tdas<T> ApiType;
    typedef typename ApiType::Architecture Arch;
    typedef typename tdas::test::TdasTesterTraits< Arch, typename ApiType::ArchitectureCapability, T> BaseT;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace tdas
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace tdas {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits<float>, cuda::test::CudaTraits<double>> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, TdasTester, CudaTraitsTypes);

} // namespace test
} // namespace tdas
} // namespace cheetah
} // namespace ska