#include "cheetah/tdao/cuda/Tdao.cuh"


namespace ska {
namespace cheetah {
namespace tdao {
namespace cuda {

Tdao::Tdao(Config const& config, tdao::Config const& algo_config)
	: utils::AlgorithmBase<Config,tdao::Config>(config,algo_config)
{
}

Tdao::~Tdao()
{
}


void Tdao::_prepare(std::size_t size)
{
    _idxs.resize(size);
    _powers.resize(size);
    _h_idxs.resize(size);
    _h_powers.resize(size);
}


} // namespace cuda
} // namespace tdao
} // namespace cheetah
} // namespace ska
