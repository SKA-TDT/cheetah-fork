#include "cheetah/tdao/cuda/Tdao.cuh"
#include "cheetah/data/Candidate.h"
#include "cheetah/data/Units.h"
#include "cheetah/cuda_utils/nvtx.h"
#include "panda/Log.h"

#include <iostream>

namespace ska {
namespace cheetah {
namespace tdao {
namespace cuda {
namespace detail {

template <typename T>
struct GreaterThanThreshold: thrust::unary_function<thrust::tuple<unsigned,T>,bool>
{
    public:
        GreaterThanThreshold(float thresh);
        inline __host__ __device__
        bool operator()(thrust::tuple<int,T> t);

    private:
        float _threshold;
 };

template <typename T>
GreaterThanThreshold<T>::GreaterThanThreshold(float threshold)
    : _threshold(threshold)
{
}

template <typename T>
inline __host__ __device__
bool GreaterThanThreshold<T>::operator()(thrust::tuple<int,T> t)
{
    return thrust::get<1>(t) > _threshold;
}

} //namespace detail

using boost::math::complement;
using boost::math::quantile;
using boost::math::cdf;

template <typename T>
std::size_t Tdao::_execute(typename thrust::device_vector<T>::const_iterator in, std::size_t size, size_t offset, float threshold)
{

    typedef thrust::tuple<unsigned,T> PeakType;
    typedef thrust::device_vector<unsigned>::iterator IdxIterator;
    typedef typename thrust::device_vector<T>::iterator PowerIterator;
    typedef thrust::tuple< thrust::counting_iterator<unsigned>, PowerIterator> PeakTupleInputIterator;
    typedef typename thrust::tuple< IdxIterator, PowerIterator > PeakTupleOutputIterator;

    using thrust::tuple;
    using thrust::counting_iterator;
    using thrust::zip_iterator;

    counting_iterator<unsigned> index_counter(offset);
    auto input_zip = make_zip_iterator(make_tuple(index_counter,in));
    auto output_zip = make_zip_iterator(make_tuple(_idxs.begin(),_powers.begin()));

#ifdef CUDA_BROKEN_THRUST_WITH_ALLOCATOR
    int num_copied = thrust::copy_if(thrust::cuda::par, input_zip, input_zip+size, output_zip,
        detail::GreaterThanThreshold<T>(threshold)) - output_zip;
#else
    int num_copied = thrust::copy_if(thrust::cuda::par(_allocator), input_zip, input_zip+size, output_zip,
        detail::GreaterThanThreshold<T>(threshold)) - output_zip;
#endif
    return (std::size_t) num_copied;
}

template <typename PowerSeriesType>
data::Ccl::CandidateType Tdao::_make_candidate(
    unsigned idx,
    float power,
    PowerSeriesType const& input,
    data::DedispersionMeasureType<float> dm,
    double accel_fact,
    std::size_t nharmonics)
{
    double sigma = input.power_to_equiv_sigma(power);
    auto period = data::Ccl::CandidateType::MsecTimeType(input.bin_to_period(idx));
    auto pdot = data::Ccl::CandidateType::SecPerSecType(accel_fact * period.value()/1000.0);
    auto width = data::Ccl::CandidateType::MsecTimeType(period/(2.0*nharmonics));
    PANDA_LOG_DEBUG << "Candidate -> Period=" <<period<< ", Pdot="<<pdot<<", Width="<<width<<", Sigma="<<sigma;
    return data::Ccl::CandidateType(period,pdot,dm,width,sigma);
}

template <typename PowerSeriesType>
void Tdao::_filter_unique(PowerSeriesType const& input,
        data::Ccl& output,
        std::size_t num_copied,
        data::DedispersionMeasureType<float> dm,
        data::AccelerationType acc,
        std::size_t nharmonics)
{
    if (num_copied<1)
    {
        return;
    }

    //should be panda async copy when this exists
    thrust::copy(_idxs.begin(),_idxs.begin()+num_copied,_h_idxs.begin());
    thrust::copy(_powers.begin(),_powers.begin()+num_copied,_h_powers.begin());
    //synchronize stream

    output.reserve(output.size() + num_copied);
    double accel_fact = (acc / (boost::units::si::constants::codata::c * input.frequency_step())).value();

    std::size_t distance;
    std::size_t idx = 0;
    float cpeak = _h_powers[idx];
    int cpeakidx = _h_idxs[idx];
    idx++;
    std::size_t init_size = output.size();
    while(idx < num_copied)
    {
        distance = _h_idxs[idx]-_h_idxs[idx-1];
        if (distance > 1)
        {
            output.push_back(_make_candidate(cpeakidx,cpeak,input,dm,accel_fact,nharmonics));
            cpeak = _h_powers[idx];
            cpeakidx = _h_idxs[idx];
        }
        else if (_h_powers[idx] > cpeak)
        {
            cpeak = _h_powers[idx];
            cpeakidx = _h_idxs[idx];
        }
        idx++;
    }
    output.push_back(_make_candidate(cpeakidx,cpeak,input,dm,accel_fact,nharmonics));
    std::size_t final_size = output.size();
    PANDA_LOG_DEBUG << "Found " << final_size-init_size << " candidates above threshold of "
    << _algo_config.significance_threshold() <<" (DM="<< dm <<", Acc="<<acc<<", filtered)";
}

template <typename T, typename Alloc>
void Tdao::process(ResourceType& gpu,
    data::PowerSeries<Architecture,T,Alloc> const& input,
    data::Ccl& output,
    data::DedispersionMeasureType<float> dm,
    data::AccelerationType acc,
    std::size_t nharmonics)
{
    PUSH_NVTX_RANGE("cuda_Tdao_process",0);
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    _prepare(input.size());
    float power_threshold = input.equiv_sigma_to_power(_algo_config.significance_threshold());
    std::size_t end_bin = (std::size_t) (_algo_config.maximum_frequency()/input.frequency_step()).value();
    std::size_t start_bin = (std::size_t) (_algo_config.minimum_frequency()/input.frequency_step()).value();
    std::size_t size = std::min(end_bin,input.size());
    PANDA_LOG_DEBUG << "Bin range to be searched -> ("<<start_bin<<", "<<size<<")";
    PUSH_NVTX_RANGE("cuda_Tdao_process_execute_kernels",1);
    std::size_t num_copied = _execute<T>(input.begin(),size,start_bin,power_threshold);
    POP_NVTX_RANGE; // cuda_Tdao_process_execute_kernels
    PANDA_LOG_DEBUG << "Found " << num_copied << " candidates above power threshold of "
    << power_threshold <<" (DM="<< dm <<", Acc="<<acc<<", unfiltered)";
    PUSH_NVTX_RANGE("cuda_Tdao_process_filter_unique",2);
    _filter_unique(input,output,num_copied,dm,acc,nharmonics);
    POP_NVTX_RANGE; // cuda_Tdao_process_filter_unique
    POP_NVTX_RANGE; // cuda_Tdao_process
}

} // namespace cuda
} // namespace tdao
} // namespace cheetah
} // namespace ska
