/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/ddtr/astroaccelerate/DdtrProcessor.h"
#include "cheetah/data/FrequencyTime.h"

namespace astroaccelerate {
extern
void bin_gpu(unsigned short *const d_input, float *const d_output, const int nchans, const int nsamp);
} // namespace astroaccelerate

namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {

template<typename DdtrTraits>
DdtrProcessor<DdtrTraits>::DdtrProcessor(DedispersionStrategyType const& strategy
                                        , data::FrequencyTime<cheetah::Cuda, uint16_t>& data
                                        , std::shared_ptr<DmTrialsType> dm_trials_ptr
                                        , bool copy_flag
                                        , size_t& current_dm_range
                                        , size_t& current_time_slice
                            )
    : _gpu_strategy(strategy)
    , _device_ft_data(data)
    , _strategy(strategy)
    , _dm_trials_ptr(dm_trials_ptr)
    , _copy_dm_trials(copy_flag)
    , _processed_samples(0)
    , _current_dm_range(current_dm_range)
    , _current_time_slice(current_time_slice)
{
    _current_dm_range = 0;
    update_dm_range();
}

template<typename DdtrTraits>
inline
void DdtrProcessor<DdtrTraits>::update_dm_range()
{
    _t_processed_range = &(_strategy.t_processed()[_current_dm_range]);
    _local_maxshift = _strategy.maxshift() / _strategy.in_bin()[_current_dm_range];
    _local_tsamp = _strategy.tsamp().value() * _strategy.in_bin()[_current_dm_range];
    _current_time_slice=0;
}

template<typename DdtrTraits>
bool DdtrProcessor<DdtrTraits>::finished() const
{
    return _current_dm_range >= _strategy.range();
}

template<typename DdtrTraits>
typename DdtrProcessor<DdtrTraits>::GpuStrategyType& DdtrProcessor<DdtrTraits>::gpu_strategy()
{
    return _gpu_strategy;
}

template<typename DdtrTraits>
typename DdtrProcessor<DdtrTraits>::DedispersionStrategyType const& DdtrProcessor<DdtrTraits>::dedispersion_strategy() const
{
    return _strategy;
}

template<typename DdtrTraits>
std::size_t const& DdtrProcessor<DdtrTraits>::current_dm_range() const
{
    return _current_dm_range;
}

template<typename DdtrTraits>
std::size_t const& DdtrProcessor<DdtrTraits>::current_time_slice() const
{
    return _current_time_slice;
}

template<typename DdtrTraits>
data::DimensionIndex<data::Time> const& DdtrProcessor<DdtrTraits>::processed_samples() const
{
    return _processed_samples;
}

template<typename DdtrTraits>
DdtrProcessor<DdtrTraits>& DdtrProcessor<DdtrTraits>::operator++()
{
    update_dm_range();
    _gpu_strategy.set_iteration(_strategy.t_processed()[_current_dm_range][_current_time_slice], (int)_local_maxshift);

    if(_current_dm_range!=0)
    {
        ::astroaccelerate::bin_gpu(&(*_device_ft_data.begin())
            , thrust::raw_pointer_cast(&*_gpu_strategy.ddtr_output().begin())
            , _device_ft_data.number_of_channels()
            , _device_ft_data.number_of_spectra()/pow(2, _current_dm_range-1)
            );
    }

    _strategy.exec_kernel(_current_dm_range
                        , _device_ft_data
                        , thrust::raw_pointer_cast(&*_gpu_strategy.ddtr_output().begin())
                        , (int)(_strategy.t_processed()[_current_dm_range][_current_time_slice])
                        , _local_tsamp
                        );
    DmTrialsType& dmtrials = *(_dm_trials_ptr);

    if(_copy_dm_trials==true)
    {
        for(std::size_t dmidx=0; dmidx<dmtrials.size(); ++dmidx)
        {
            panda::copy(_gpu_strategy.ddtr_output().begin() + dmtrials.metadata().number_of_samples()*dmidx
                      , _gpu_strategy.ddtr_output().begin() + dmtrials.metadata().number_of_samples()*(dmidx+1)-1
                      , dmtrials[dmidx].begin());
        }
    }

    _processed_samples = data::DimensionIndex<data::Time>(_strategy.t_processed()[0][0]);

    return *this;
}

} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska
