set(MODULE_GPU_BRUTEFORCE_LIB_SRC_CPU
    src/Config.cpp
    ${LIB_SRC_CPU}
    PARENT_SCOPE
)

set(MODULE_GPU_BRUTEFORCE_LIB_SRC_CUDA
    src/DedispersionStrategy.cu
    src/Kernel.cu
    PARENT_SCOPE
)

add_subdirectory(test)
