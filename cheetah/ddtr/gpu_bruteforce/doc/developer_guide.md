@page gpu_bruteforce_developer_guide GPU bruteforce developerguide
# Notes for Developers
## GPU kernels

This module provides two CUDA kernels:
- the DDTR kernel for dedispersion it is bruteforce DDTR kernel is simple kernel which does not use the shared memory but relies on the GPU cache.
- GPU binning kernel used to integrate FrequencyTime data.

Both the kernels can be found in the detail/kernels directory.

## Constants Used
The algorithm works on all nvidia gpus with compute capability 3.5 and up. There are some constants hardcoded in that reflect the card limitations. These can be found in
detail/GpuConstraints.h and are used in:
- detail/DedispersionStrategy.cpp
- Dmshifts are stored in the constant memory and defined in /detail/kernels/gpu_brute_force_kernel.cu

This should work on all the NVIDIA GPUs released after 2015.

## Instantiating Kernel and DdtrStrategy objects
Various CUDA template codes need to be explicitly instantiated with the value_type of the incoming data in order to be included in the cheetah library.
This allows them to be used in c++ code.

Instead of having a single place to define these types the developer needs to ensure
the following types are updated and consistent with the required set of value_types:

- Kernel objects are instantiated in src/Kernel.cu
- DdtrStrategy objects are instantiated in src/DdtrStrategy.cu
- Unit tests are set in
    - test/src/DdtrTest.cpp
    - test/src/DedispersionStrategyTest
