@page gpu_bruteforce_algorithm_guide GPU bruteforce algorithm guide
# Notes for Developers
## DDTR algorithm
This is a simple algorithm that works by adding bins along a predicted dm curve. No attempt is made to reduce the overall number of computations by sharing additions along concurrent curves.

There three steps that are followed after the Buffering stage (where the TF chunks are aggregated to form a one single buffer which can be transfered to GPU)
- Cornerturn
- Binning
- DDTR

### Cornerturn
One of the important factor for GPUs performance is global memory access. To reduce the latencies involved in memory access data should be arranged in Frequency-Time format. The is marked increase in performance taking advantage of  due to memory coalescing. So, we need to cornerturn TF block to FT block.

### Binning
Full time resolution for all the DM ranges. The user can select to reduce the time resolution and integrate the data in time. `bin_gpu` method can perform this task on GPUs by executing the kernel in `detail/kernels/batched_gpu_bin.cu`

### DDTR
Here we transform the data from Frequency-Time plane to DM-Time plane. This kernel relys the onchip cache to coalesce the memory access. This should technically run as fast as the kernels which use shared memory on latest GPUs. The kernel is located in `detail/kernels/gpu_brute_force_kernel.cu`.
