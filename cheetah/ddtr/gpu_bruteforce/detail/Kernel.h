/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_KERNEL_H
#define SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_KERNEL_H

#include "cheetah/data/cuda/FrequencyTime.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace gpu_bruteforce {

template<class, typename> class DedispersionStrategy;

/**
 * @brief Template for generating optimised Cache level Cuda kernels
 * @tparam NumericalT: data type of the elements in the FrequencyTime block.
 */
template<typename NumericalT>
class Kernel
{
    public:
        /**
         * @brief exec method to execute the kernels
         * @param current_dm_range the index of the DM range that must be processed.
         * @param strategy DDTR strategy object
         * @param d_input FT object which points to the data on GPU
         * @param d_output pointer to the memory location on GPU where the DMtrails should be written
         * @param t_processed: total dedispersed samples per dm expected
         * @param bin Binning needed for the this particular DM range
         * @param threads_per_block 1024 for all the latest GPUs till now passed as constant
         */
        static void exec(int dm_range
                       , DedispersionStrategy<Cpu, NumericalT> const& strategy
                       , data::FrequencyTime<Cuda, NumericalT> const& d_input
                       , float* d_output
                       , int t_processed
                       , int bin
                       , const int threads_per_block
                       );

        /**
        * @brief integrates data in time by adding the adjecent tiem samples and halves the data size.
        */
        static void bin_gpu( data::FrequencyTime<Cuda, NumericalT>& d_input, int nelements, int bin, const int threads_per_block);
};


} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_KERNEL_H
