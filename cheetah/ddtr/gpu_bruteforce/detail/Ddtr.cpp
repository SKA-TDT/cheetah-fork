/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/ddtr/gpu_bruteforce/detail/DdtrWorker.h"
#include "cheetah/ddtr/gpu_bruteforce/DdtrProcessor.h"
#include "cheetah/ddtr/gpu_bruteforce/Ddtr.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace gpu_bruteforce {

template<typename DdtrTraits>
struct Ddtr<DdtrTraits>::DdtrWorkerFactory
{
    DdtrWorkerFactory(Config const& config, std::shared_ptr<DedispersionPlan> const& plan)
        : _plan(plan)
        , _config(config)
    {}

    template<typename DeviceType>
    DdtrWorker<DdtrTraits>* operator()(DeviceType&)
    {
        return new DdtrWorker<DdtrTraits>(_config);
    }

    private:
        std::shared_ptr<DedispersionPlan> _plan;
        Config const& _config;
};

template<typename DdtrTraits>
Ddtr<DdtrTraits>::Ddtr(ddtr::Config const& config)
    : _plan(std::make_shared<DedispersionPlan>(config, 0))
    , _factory(new DdtrWorkerFactory(config.gpu_bruteforce_algo_config(), _plan))
    , _workers(*_factory)
{
}

template<typename DdtrTraits>
Ddtr<DdtrTraits>::Ddtr(Ddtr&& other)
    : _plan(std::move(other._plan))
    , _factory(std::move(other._factory))
    , _workers(std::move(other._workers))
{
}

template<typename DdtrTraits>
template<typename CallBackT>
std::shared_ptr<typename Ddtr<DdtrTraits>::DmTrialsType> Ddtr<DdtrTraits>::operator()(panda::PoolResource<cheetah::Cuda>& gpu, BufferType const& data, CallBackT const& call_back)
{
    return _workers(gpu)(gpu, data, _plan, call_back);
}

template<typename DdtrTraits>
std::shared_ptr<typename Ddtr<DdtrTraits>::DmTrialsType> Ddtr<DdtrTraits>::operator()(panda::PoolResource<cheetah::Cuda>& gpu, BufferType const& data)
{
    return (*this)(gpu, data, [](DdtrProcessor<DdtrTraits> const&){});
}

template<typename DdtrTraits>
void Ddtr<DdtrTraits>::plan(DedispersionPlan const& plan)
{
    _plan = std::make_shared<DedispersionPlan>(plan);
}

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace cheetah
} // namespace ska
