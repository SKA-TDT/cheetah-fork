/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DEDISPERSIONSTRATEGY_CUH
#define SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DEDISPERSIONSTRATEGY_CUH

#include "cheetah/ddtr/gpu_bruteforce/DedispersionStrategy.h"
#include "cheetah/data/TimeSeries.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace gpu_bruteforce {

/**
* @brief Cuda specialization of Dedispersion strategy holds the time series oject
*/
template<typename NumericalRep>
class DedispersionStrategy<Cuda, NumericalRep>
{
    public:
        /**
        * @brief constructor to allocate memory for the Timeseries object
        */
        DedispersionStrategy(DedispersionStrategy<Cpu, NumericalRep> const&);

        /**
        * @brief returns reference to the TimeSeries object
        */
        data::TimeSeries<cheetah::Cuda, float>& ddtr_output();

    private:
        std::unique_ptr<data::TimeSeries<cheetah::Cuda, float>> _d_out;
};


}// gpu_bruteforce
}// ddtr
}// cheetah
}// ska

#endif // SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DEDISPERSIONSTRATEGY_CUH
