/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DDTR_H
#define SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DDTR_H

#include "cheetah/Configuration.h"
#ifdef SKA_CHEETAH_ENABLE_CUDA

#include "cheetah/ddtr/gpu_bruteforce/DedispersionPlan.h"
#include "cheetah/ddtr/Config.h"
#include <panda/DeviceLocal.h>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace gpu_bruteforce {

/**
 * @brief A GPU brute force algorithm to perform DDTR.
 * @details For implementation details please look in the \subpage gpu_bruteforce_developer_guide "developer guide"
 *          More general information on the algorithm can be found in the \subpage gpu_bruteforce_algorithm_guide "algorithm guide".
 */

template<typename DdtrTraits>
class Ddtr
{
    private:
        typedef typename DdtrTraits::DedispersionHandler DedispersionHandler;
        typedef typename DdtrTraits::DmTrialsType DmTrialsType;
        typedef ddtr::Config::Dm Dm;
        typedef typename DdtrTraits::BufferFillerType BufferFillerType;
        typedef typename DdtrTraits::BufferType BufferType;
        typedef std::vector<ddtr::Config::Dm> DmListType;

    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<3, 5, panda::nvidia::giga> ArchitectureCapability;

        typedef gpu_bruteforce::DedispersionPlan<DdtrTraits> DedispersionPlan;
        typedef gpu_bruteforce::Config Config;
        typedef typename DdtrTraits::value_type NumericalRep;
        typedef typename DdtrTraits::TimeFrequencyType TimeFrequencyType;
        typedef typename TimeFrequencyType::FrequencyType FrequencyType;
        typedef typename TimeFrequencyType::TimeType TimeType;
        typedef std::vector<FrequencyType> FrequencyListType;

    public:
        Ddtr(ddtr::Config const& config);
        Ddtr(Ddtr const&) = delete;
        Ddtr(Ddtr&&);

        /**
         * @brief dedispersion of time frequency data on GPU
         * @details DmTialsHandler called when buffer is full.
         * @param data   A TimeFrequency block and cpu resource
         * @tparam call_back A functor called after the dedispersion has been calculated for a specific range of DMs.
         *         The call_back should support the signature void(DdtrProcessor<DdtrTraits> const&)
         *         It allows further processing on the range data while it is still in place on the GPU device
         * @return DmTime sequence i.e. timeseries for each DM trial value
         */
        template <typename CallBackT>
        std::shared_ptr<DmTrialsType> operator()(panda::PoolResource<cheetah::Cuda>& gpu
                                                , BufferType const& data
                                                , CallBackT const& call_back);

        /**
         * @brief dedispersion of time frequency data on GPU
         * @details DmTrialsHandler called when buffer is full.
         * @param data  A TimeFrequency block or FrequencyType
         * @return DmTime sequence i.e. timeseries for each DM trial value
         */
        std::shared_ptr<DmTrialsType> operator()(panda::PoolResource<cheetah::Cuda>& gpu, BufferType const& data);

        /**
         * @brief sets plan for ddtr
         */
        void plan(DedispersionPlan const& plan);

    private:
        struct DdtrWorkerFactory;

    private:
        std::shared_ptr<DedispersionPlan> _plan;
        std::unique_ptr<DdtrWorkerFactory> _factory;
        panda::DeviceLocal<panda::PoolResource<cheetah::Cuda>, DdtrWorkerFactory> _workers;
};

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace cheetah
} // namespace ska
#include "detail/Ddtr.cpp"

#endif //SKA_CHEETAH_ENABLE_CUDA
#endif // SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DDTR_H
