include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_tdrt_cuda_src
    src/TdrtTest.cu
    src/gtest_tdrt_cuda.cu
)

if(ENABLE_CUDA)
    cuda_add_executable(gtest_tdrt_cuda ${gtest_tdrt_cuda_src})
    target_link_libraries(gtest_tdrt_cuda ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
    add_test(gtest_tdrt_cuda gtest_tdrt_cuda --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
endif()
